from django import forms
from django.conf import settings
from django.contrib.auth.forms import (
    UserCreationForm as AuthUserCreationForm, PasswordResetForm as AuthPasswordResetForm,
)
from django.forms.models import ModelChoiceIterator
from django.template import loader

from orchard.mail import send_mail
from .mailchimp import MailChimpList
from .models import User


class LabelSortedModelChoiceIterator(ModelChoiceIterator):
    def __iter__(self):
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label)
        queryset = sorted(list(self.queryset.all()), key=lambda obj: obj.name.lower())
        for obj in queryset:
            yield self.choice(obj)


class LabelSortedModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    iterator = LabelSortedModelChoiceIterator
    widget = forms.CheckboxSelectMultiple


class UserFormMixin:
    class Meta:
        model = User
        fields = (
            "first_name", "last_name", "email", "language", "street", "pcode",
            "city", "phone", "paiement_info", "receive_news", "fruit_interest",
        )
        field_classes = {
            'fruit_interest': LabelSortedModelMultipleChoiceField,
        }

    def __init__(self, **kwargs):
        typ = kwargs.pop('typ')
        super().__init__(**kwargs)
        if typ != 'owner':
            del self.fields['paiement_info']
        self.fields['last_name'].required = True


class UserCreationForm(UserFormMixin, AuthUserCreationForm):
    def save(self, commit=True):
        user = super().save(commit)
        self.save_m2m()
        if user.receive_news:
            MailChimpList().add_member(user)
        return user


class UserChangeForm(UserFormMixin, forms.ModelForm):
    next_page = forms.CharField(widget=forms.HiddenInput(), required=False)

    def save(self, **kwargs):
        if 'receive_news' in self.changed_data:
            if self.cleaned_data['receive_news']:
                MailChimpList().add_member(self.instance)
            else:
                MailChimpList().remove_member(self.instance.email)
        elif self.instance.receive_news and 'email' in self.changed_data:
            old_email = User.objects.get(pk=self.instance.pk).email
            MailChimpList().update_member(self.instance, old_email, self.cleaned_data['email'])
        return super().save(**kwargs)


class PasswordResetForm(AuthPasswordResetForm):
    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        # Customized from here
        send_mail(subject, body, settings.SITE_EMAIL, [to_email])

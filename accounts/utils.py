import base64
import binascii

from django.core.signing import BadSignature, TimestampSigner

BOOKING_EMAIL_VERIFY_TIMEOUT_DAYS = 7


class EmailVerifyTokenGenerator(object):
    """
    From: https://bitbucket.org/spookylukey/cciw-website/src/default/cciw/bookings/email.py
    Strategy object used to generate and check tokens for the email verification
    mechanism.
    """
    def __init__(self):
        self.signer = TimestampSigner(salt="obstgarten.accounts.EmailVerifyTokenGenerator")

    def token_for_email(self, email):
        """
        Returns a verification token for the provided email address
        """
        return base64.urlsafe_b64encode(
            self.signer.sign(email).encode('utf-8')).decode('utf-8')

    def email_for_token(self, token):
        """
        Extracts the verified email address from the token, or None if verification failed
        """
        try:
            return self.signer.unsign(
                base64.urlsafe_b64decode(token.encode('utf-8')).decode('utf-8'),
                max_age=BOOKING_EMAIL_VERIFY_TIMEOUT_DAYS * 60 * 60 * 24
            )
        except (binascii.Error, BadSignature, UnicodeDecodeError):
            return None

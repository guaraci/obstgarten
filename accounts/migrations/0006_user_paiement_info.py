from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0005_user_language'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='paiement_info',
            field=models.TextField(blank=True, help_text='CCP, Bank account, IBAN… to receive paiements if you charge for fruits', verbose_name='Paiement info'),
        ),
    ]

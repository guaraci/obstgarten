from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0007_fruite_interest_to_treespecies'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='fruit_interests',
        ),
    ]

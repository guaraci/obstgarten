from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0023_treespecies_color'),
        ('accounts', '0006_user_paiement_info'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='fruit_interest',
            field=models.ManyToManyField(blank=True, to='orchard.TreeSpecies', verbose_name='Fruit interests'),
        ),
    ]

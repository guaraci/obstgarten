from django.conf import settings
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _


class MyUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)
    email_confirmed = models.BooleanField(default=False)
    language = models.CharField(_("Language"), max_length=5, choices=settings.LANGUAGES)
    street = models.CharField(_("Street"), max_length=150, blank=True)
    pcode = models.CharField(_("Postal code"), max_length=5)
    city = models.CharField(_("Locality"), max_length=50)
    phone = models.CharField(_("Phone"), max_length=30, blank=True)
    paiement_info = models.TextField(_("Paiement info"), blank=True,
        help_text=_("CCP, Bank account, IBAN… to receive paiements if you charge for fruits"))
    receive_news = models.BooleanField(
        _("I am interested to receive news from this site"), default=True
    )
    fruit_interest = models.ManyToManyField('orchard.TreeSpecies', blank=True, verbose_name=_("Fruit interests"))

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        if self.first_name or self.last_name:
            return " ".join([self.first_name, self.last_name])
        return self.email

    @cached_property
    def is_owner(self):
        return self.orchard_set.exists()

    def deletable(self):
        # FIXME: currently, those links are protected, they should be 'serialized'
        # when the user object is deleted.
        return not self.booking_set.exists() and not self.productorder_set.exists()

    def get_address(self):
        """Concatenated street, pcode, city."""
        city = " ".join([self.pcode, self.city]).strip()
        if self.street:
            return "%s\n%s" % (self.street, city)
        else:
            return city

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.db.models import Count
from django.http import HttpResponse
from django.utils.translation import gettext_lazy as _

from .models import User

@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = [
        'email', 'last_name', 'first_name', 'pcode', 'city', 'language', 'last_login',
        'num_orchards', 'num_searches', 'receive_news',
    ]
    list_filter = ['receive_news']
    ordering = ('email',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name', 'last_name', 'email_confirmed', 'language', 'street',
            ('pcode', 'city'), 'phone', 'paiement_info', 'receive_news', 'fruit_interest'
            )}
        ),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    readonly_fields = ('date_joined', 'last_login')
    search_fields = ['email', 'last_name', 'first_name']
    actions = ['get_emails']

    def get_queryset(self, request):
        return super().get_queryset(request).annotate(
            num_orchards=Count('orchard'), num_searches=Count('savedsearch')
        )

    def num_orchards(self, obj):
        return obj.num_orchards or '-'
    num_orchards.short_description = _("Orchards")

    def num_searches(self, obj):
        return obj.num_searches or '-'
    num_searches.short_description = _("Saved searches")

    def get_emails(self, request, queryset):
        return HttpResponse(", ".join(queryset.values_list('email', flat=True)), content_type='text/plain')
    get_emails.short_description = _("Export emails")

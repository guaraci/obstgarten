from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.views import LoginView as AuthLoginView
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse, reverse_lazy
from django.utils.http import url_has_allowed_host_and_scheme
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy, gettext as _

from django.views.generic import CreateView, DeleteView, ListView, UpdateView, View

from orchard.mail import send_mail
from orchard.views import TitleViewMixin

from . import forms
from .mailchimp import MailChimpList
from .models import User
from .utils import EmailVerifyTokenGenerator


class LoginView(TitleViewMixin, AuthLoginView):
    template_name = 'accounts/login.html'
    title = gettext_lazy("Login")


class CreateAccountView(CreateView):
    form_class = forms.UserCreationForm
    template_name = 'accounts/account.html'
    typ = None

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('home'))
        return super().get(request)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['typ'] = self.typ
        return kwargs

    def form_valid(self, form):
        response = super().form_valid(form)
        # Send confirmation email
        context = {
            'domain': get_current_site(self.request).domain,
            'token': EmailVerifyTokenGenerator().token_for_email(self.object.email),
            'protocol': 'https' if self.request.is_secure() else 'http',
        }
        body = loader.render_to_string("accounts/verification_email.txt", context, using='django-text')
        subject = _("MeinObstgarten email confirmation")
        send_mail(subject, body, settings.SITE_EMAIL, [self.object.email])
        return response

    def get_success_url(self):
        messages.success(self.request,
            _("Thanks for creating the account. An email has been sent to %s, "
              "please check your mailbox and follow the given link to confirm "
              "your address and be able to login on this site.") % self.object.email)
        return reverse('offer')


class OnOwnProfileMixin(UserPassesTestMixin):
    """
    On most profile pages, only the logged-in user or a superuser can access the
    page.
    """
    def test_func(self):
        return self.request.user.is_superuser or int(self.kwargs['pk']) == self.request.user.pk


class EditAccountView(OnOwnProfileMixin, UpdateView):
    model = User
    form_class = forms.UserChangeForm
    template_name = 'accounts/account.html'

    def get_initial(self):
        initial = super().get_initial()
        if not 'next_page' in self.initial:
            initial['next_page'] = self.request.META.get('HTTP_REFERER', "/")
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['typ'] = 'owner' if self.object.is_owner else 'user'
        return kwargs

    def form_valid(self, form):
        self.object = form.save()
        next_url = form.cleaned_data['next_page']
        if url_has_allowed_host_and_scheme(next_url, allowed_hosts=[self.request.get_host()]):
            return HttpResponseRedirect(next_url)
        return HttpResponseRedirect(reverse('home'))


class VerifyAccountView(View):
    def get(self, request, *args, **kwargs):
        # Verify token, set user.email_confirmed, redirect with message
        email = EmailVerifyTokenGenerator().email_for_token(kwargs['token'])
        if email is None:
            messages.error(request, _("Sorry, the account verification failed."))
        else:
            account = get_object_or_404(User, email=email)
            account.email_confirmed = True
            account.save()
            login(request, account)
            messages.success(request, _("Your account was successfully confirmed!"))
        return HttpResponseRedirect(reverse('home'))


class DeleteAccountView(OnOwnProfileMixin, DeleteView):
    model = User
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        user = self.object
        # Test and warn if user has orchard/product order/booking linked.
        if user.orchard_set.count() > 0:
            messages.error(self.request, _("Sorry, you must delete your orchards before you can delete your account."))
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', "/"))
        if user.productorder_set.filter(status='ordered').count() > 0:
            messages.error(
                self.request,
                mark_safe(_('Sorry, you have <a href="%s">pending orders</a>. Please cancel '
                  'them before you can delete your account.') % reverse(
                    'productorders-list', args=[user.pk]
                ))
            )
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', "/"))
        if user.booking_set.filter(status='planned').count() > 0:
            messages.error(
                self.request,
                mark_safe(_('Sorry, you have planned <a href="%s">tree bookings</a>. '
                  'Please cancel them before you can delete your account.') % reverse(
                    'bookings-list', args=[user.pk]
                ))
            )
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', "/"))
        # Archive product orders
        for order in user.productorder_set.all():
            order.archive(reason='User deleted his/her account')
        # Archive bookings
        for booking in user.booking_set.all():
            booking.archive(reason='User deleted his/her account')
        if user.email and user.receive_news:
            MailChimpList().remove_member(user.email)
        response = super().form_valid(form)
        messages.success(self.request, _("Your account was successfully deleted."))
        return response


class SavedSearchList(OnOwnProfileMixin, ListView):
    template_name = 'accounts/savedsearch_list.html'

    def get_queryset(self):
        return self.request.user.savedsearch_set.all()


class BookingsList(OnOwnProfileMixin, ListView):
    template_name = 'accounts/bookings_list.html'

    def get_queryset(self):
        return self.request.user.booking_set.all().select_related('tree__orchard')


class ProductOrdersList(OnOwnProfileMixin, ListView):
    template_name = 'accounts/productorders_list.html'

    def get_queryset(self):
        return self.request.user.productorder_set.all().order_by('status')


def set_language(request, lang_code):
    """
    Inspired from django.views.i18n and adapted to accept REST parameter
    """
    next_url = request.POST.get('next', request.GET.get('next'))
    if not next_url or not url_has_allowed_host_and_scheme(
        next_url,
        allowed_hosts={request.get_host()},
    ):
        next_url = request.META.get('HTTP_REFERER', "/")
    response = HttpResponseRedirect(next_url)

    response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
    if request.user.is_authenticated:
        request.user.language = lang_code
        request.user.save()
    return response

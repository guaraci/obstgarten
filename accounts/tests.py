import re
from unittest import mock

from django.conf import settings
from django.core import mail
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import get_language, gettext as _
from django.test import TestCase, override_settings

from accounts.models import User
from orchard.models import (
    Booking, BookingArchived, FruitCategory, Orchard, Product, ProductOrder,
    ProductOrderArchived, TreeSpecies,
)
from orchard.tests import TestDataMixin


def mocked_requests(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data
    return MockResponse(None, 200)


@override_settings(
    MAILCHIMP_APIUSER='me', MAILCHIMP_APIKEY='xxxxx-us16', MAILCHIMP_LISTID='123456'
)
class AccountTests(TestDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.lang = get_language()
        fc = FruitCategory.objects.language(cls.lang).create(name='fruit categ')
        TreeSpecies.objects.language(cls.lang).create(category=fc, name='Apple')
        TreeSpecies.objects.language(cls.lang).create(category=fc, name='Cherry')
        TreeSpecies.objects.language(cls.lang).create(category=fc, name='Plum')

    def test_create_account(self):
        response = self.client.get(reverse('create_account_user'))
        self.assertNotContains(response, 'id="id_paiement_info"')
        post_data = {
            'first_name': "Oscar",
            'last_name': "Racso",
            'email': "oscar@example.org",
            'language': 'de',
            'pcode': "4321",
            'city': "SomeTown",
            'receive_news': "on",
            'fruit_interest': [
                str(ts) for ts in TreeSpecies.objects.translated(self.lang
                    ).filter(translations__name__in=['Apple', 'Cherry']).values_list('pk', flat=True)
            ],
            'password1': "sghz%er*",
            'password2': "sghz%er*",
        }
        with mock.patch('requests.post', side_effect=mocked_requests) as mocked:
            response = self.client.post(reverse('create_account_user'), data=post_data, follow=True)
        self.assertContains(response, "oscar@example.org")  # Inside the success message
        self.assertEqual(
            mocked.call_args[1]['json'], {
                'email_address': 'oscar@example.org',
                'merge_fields': {
                    'FNAME': 'Oscar',
                    'LNAME': 'Racso',
                },
                'language': 'de',
                'status': 'subscribed',
            }
        )
        user = User.objects.get(email="oscar@example.org")
        self.assertTrue(user.receive_news)
        self.assertFalse(user.email_confirmed)
        self.assertQuerySetEqual(
            user.fruit_interest.all().order_by('id'),
            ['<TreeSpecies: Apple>', '<TreeSpecies: Cherry>'],
            transform=repr
        )
        # Test email sent
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, _("MeinObstgarten email confirmation"))
        # Test verification from URL inside email
        m = re.search(r'(http://.*)', mail.outbox[0].body)
        url = m.group()
        response = self.client.get(url, follow=True)
        self.assertContains(response, _("Your account was successfully confirmed!"))
        user.refresh_from_db()
        self.assertTrue(user.email_confirmed)

    def test_modify_account(self):
        user = User.objects.create_user(
            'jane@example.org', password='sghz%er*', receive_news=False
        )
        user_data = {
            'first_name': "Jane",
            'last_name': "Smith",
            'email': "jane@example.org",
            'language': 'fr',
            'pcode': "1000",
            'city': "Lausanne",
            'receive_news': "on",
            'fruit_interest': [
                str(ts) for ts in TreeSpecies.objects.translated(self.lang
                    ).filter(translations__name__in=['Apple', 'Cherry']).values_list('pk', flat=True)
            ],
            'next_page': reverse('offer'),
        }
        edit_url = reverse('edit_account', args=[user.pk])
        response = self.client.post(edit_url, data=user_data)
        self.assertRedirects(response, reverse('login') + '?next=%s' % edit_url)
        self.client.login(email=user.email, password='sghz%er*')
        with mock.patch('requests.post', side_effect=mocked_requests) as mocked:
            response = self.client.post(edit_url, data=user_data)
        self.assertRedirects(response, reverse('offer'))
        self.assertEqual(
            mocked.call_args[1]['json'], {
                'email_address': 'jane@example.org',
                'merge_fields': {
                    'FNAME': 'Jane',
                    'LNAME': 'Smith',
                },
                'language': 'fr',
                'status': 'subscribed',
            }
        )
        user_data['email'] = 'jill@example.org'
        with mock.patch('requests.patch', side_effect=mocked_requests) as mocked:
            response = self.client.post(edit_url, data=user_data)
        self.assertEqual(
            mocked.call_args[1]['json'], {
                'email_address': 'jill@example.org',
                'merge_fields': {
                    'FNAME': 'Jane',
                    'LNAME': 'Smith',
                },
            }
        )

        # user language changed site language
        response = self.client.get(reverse('offer'))
        self.assertContains(response, '<h2 class="site-hero__subtitle">Offre de fruits</h2>')
        user.refresh_from_db()
        self.assertTrue(user.receive_news)
        self.assertQuerySetEqual(
            user.fruit_interest.all().order_by('id'),
            ['<TreeSpecies: Apple>', '<TreeSpecies: Cherry>'],
            transform=repr
        )

    def test_delete_account(self):
        user = User.objects.create_user('jane@example.org', password='sghz%er*')
        Orchard.objects.create(
            owner=user, name="Test private", municipality=self.location, is_public=False,
        )
        self.client.login(email=user.email, password='sghz%er*')
        delete_url = reverse('delete-account', args=[user.pk])
        response = self.client.post(delete_url, data={}, follow=True)
        self.assertContains(
            response,
            _("Sorry, you must delete your orchards before you can delete your account.")
        )
        user.orchard_set.all().delete()
        with mock.patch('requests.delete', side_effect=mocked_requests) as mocked:
            response = self.client.post(delete_url, data={}, follow=True)
        self.assertContains(response, _("Your account was successfully deleted."))
        self.assertTrue(mocked.called)
        self.assertFalse(User.objects.filter(pk=user.pk).exists())

    def test_delete_account_with_products(self):
        """Test user deletion behaviour with pending product orders."""
        user = User.objects.create_user('jane@example.org', password='sghz%er*')
        product = Product.objects.create(
            orchard=self.orchard, title="My product", availability=True, price='8.45',
            sending_costs='2.5',
        )
        order = ProductOrder.objects.create(
            user=user, product=product, total_price='8.45', order_date=timezone.now(),
            status='ordered'
        )
        self.client.force_login(user)
        response = self.client.post(reverse('delete-account', args=[user.pk]), data={}, follow=True)
        self.assertContains(
            response,
            _('Sorry, you have <a href="%s">pending orders</a>. Please cancel them before you can delete your account.') % reverse('productorders-list', args=[user.pk])
        )
        order.status = 'cancelled'
        order.save()
        order_count = ProductOrder.objects.count()
        response = self.client.post(reverse('delete-account', args=[user.pk]), data={}, follow=True)
        self.assertContains(response, _("Your account was successfully deleted."))
        self.assertEqual(ProductOrder.objects.count(), order_count - 1)
        self.assertEqual(ProductOrderArchived.objects.filter(product__orchard__pk=self.orchard.pk).count(), 1)

    def test_delete_account_with_bookings(self):
        """Test user deletion behaviour with pending bookings."""
        user = User.objects.create_user('jane@example.org', password='sghz%er*')
        tree = self.add_tree(self.orchard, availability='whole')
        booking = Booking.objects.create(tree=tree, year=self.next_year, user=user, status='planned')
        self.client.force_login(user)
        response = self.client.post(reverse('delete-account', args=[user.pk]), data={}, follow=True)
        self.assertContains(
            response,
            _('Sorry, you have planned <a href="%s">tree bookings</a>. Please cancel them before you can delete your account.') % reverse('bookings-list', args=[user.pk])
        )
        booking.status = 'cancelled'
        booking.save()
        booking_count = Booking.objects.count()
        response = self.client.post(reverse('delete-account', args=[user.pk]), data={}, follow=True)
        self.assertContains(response, _("Your account was successfully deleted."))
        self.assertEqual(Booking.objects.count(), booking_count - 1)
        self.assertEqual(BookingArchived.objects.filter(tree__orchard__pk=self.orchard.pk).count(), 1)

    def test_set_language(self):
        self.client.get(reverse('set_language', args=['pt']))
        self.assertEqual(self.client.cookies[settings.LANGUAGE_COOKIE_NAME].value, 'pt')

    @override_settings(SITE_EMAIL='info@meinobstgarten.ch')
    def test_password_reset(self):
        User.objects.create_user('jane@example.org', password='sghz%er*')
        response = self.client.post(reverse('password_reset'), data={'email': 'jane@example.org'})
        self.assertRedirects(response, reverse('password_reset_done'))
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].from_email, settings.SITE_EMAIL)

import hashlib
import json
import requests

from django.conf import settings
from django.core.mail import mail_admins


class MailChimpList:
    def __init__(self):
        self.list_id = settings.MAILCHIMP_LISTID
        self.api_user = settings.MAILCHIMP_APIUSER
        self.api_key = settings.MAILCHIMP_APIKEY
        if self.list_id:
            dc = self.api_key.rsplit('-', 1)[1]
            self.url_base = 'https://%s.api.mailchimp.com/3.0/' % dc

    def on_error(self, op, err):
        mail_admins('MailChimp error',
            "While trying to %s, we got the following response: %s" % (
                op, err
            )
        )

    def email_hash(self, email):
        return hashlib.md5(normalize(email).encode('utf-8')).hexdigest()

    def get_merge_fields(self, user):
        return {
            'FNAME': user.first_name,
            'LNAME': user.last_name,
        }

    def add_member(self, user):
        if not self.list_id:
            return
        url = '{base}lists/{list_id}/members'.format(base=self.url_base, list_id=self.list_id)
        post_data = {
            'email_address': normalize(user.email),
            'status': 'subscribed',
            'language': user.language,
            'merge_fields': self.get_merge_fields(user),
        }
        try:
            resp = requests.post(url, json=post_data, auth=(self.api_user, self.api_key))
        except Exception as err:
            self.on_error('add member %s' % user, str(err))
            return
        if resp.status_code > 299:
            try:
                response = json.loads(resp.text)
            except json.JSONDecodeError:
                self.on_error('add member %s' % user, resp.text)
                return
            if response.get('title', '') == "Member Exists":
                self.update_member(user, user.email, user.email)
                return
            self.on_error('add member %s' % user, resp.text)

    def update_member(self, user, old_email, new_email):
        if not self.list_id:
            return
        url = '{base}lists/{list_id}/members/{subscriber_hash}'.format(
            base=self.url_base, list_id=self.list_id,
            subscriber_hash=self.email_hash(old_email),
        )
        post_data = {
            'email_address': normalize(new_email),
            'merge_fields': self.get_merge_fields(user),
        }
        try:
            resp = requests.patch(url, json=post_data, auth=(self.api_user, self.api_key))
        except Exception as err:
            self.on_error('update address %s to %s' % (old_email, new_email), str(err))
            return
        if resp.status_code == 404:
            self.add_member(user)
        elif resp.status_code > 299:
            try:
                response = json.loads(resp.text)
            except json.JSONDecodeError:
                self.on_error('update address %s to %s' % (old_email, new_email), resp.text)
                return
            try:
                error_text = response['errors'][0]['message']
            except (KeyError, IndexError):
                error_text = ''
            if error_text and old_email != new_email and (
                    'You can only update email addresses for members with a status of "subscribed."' in error_text):
                self.remove_member(old_email)
                self.add_member(user)
            elif (error_text and old_email != new_email and
                    "is already in this list" in error_text):
                self.remove_member(old_email)
                self.update_member(user, new_email, new_email)
            else:
                self.on_error('update address %s to %s' % (old_email, new_email), resp.text)

    def remove_member(self, email):
        if not self.list_id:
            return
        url = '{base}/lists/{list_id}/members/{subscriber_hash}'.format(
            base=self.url_base, list_id=self.list_id, subscriber_hash=self.email_hash(email),
        )
        try:
            resp = requests.delete(url, auth=(self.api_user, self.api_key))
        except Exception as err:
            self.on_error('remove address %s' % email, str(err))
            return
        if resp.status_code > 299 and resp.status_code != 404:
            self.on_error('remove address %s' % email, resp.text)


def normalize(email):
    email = email.lower()
    try:
        email.encode('ascii')
    except UnicodeEncodeError:
        # Possible internationalized domain
        user_part, domain_part = email.rsplit('@', 1)
        return user_part + '@' + domain_part.encode('idna').decode('ascii')
    else:
        return email

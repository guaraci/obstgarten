{% blocktrans %}This is the regular email informing you about new trees available from your saved search query on the www.{{ hostname }} website.{% endblocktrans %}

{% trans "Your search:" %} https://www.{{ hostname }}{{ search_url }}

{% blocktrans %}New trees
---------{% endblocktrans %}
{% for tree in trees %} * {{ tree }} in {{ tree.orchard }}
   https://www.{{ hostname }}{{ tree.orchard.get_absolute_url }}

{% endfor %}

{% trans "If you don't want to receive new email for your search, use the link at the top and delete your search." %}

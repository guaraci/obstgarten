from django.db import models
from django.utils import timezone

from parler.managers import TranslatableManager, TranslatableQuerySet
from parler.models import TranslatableModel, TranslatedFields


class CurrentNewsManager(TranslatableManager):
    default_class=TranslatableQuerySet

    def get_queryset(self):
        return super().get_queryset(
            ).filter(pub_date__lte=timezone.now()).order_by('-pub_date')


class NewsItem(TranslatableModel):
    pub_date = models.DateTimeField()

    translations = TranslatedFields(
        content = models.TextField(),
    )

    current = CurrentNewsManager()

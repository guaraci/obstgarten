import datetime

from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView

from .models import NewsItem


class NewsListView(ListView):
    model = NewsItem
    template_name = 'news/newsitem_list.html'

    def get_queryset(self):
        year_limit = datetime.datetime.now().year - 2
        return super().get_queryset().filter(pub_date__year__gte=year_limit).order_by('-pub_date')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [(_('Home'), reverse('home'))],
            'title': _("News"),
        })
        return context

from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager
import parler.fields


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='newsitem',
            options={},
        ),
        migrations.AlterModelOptions(
            name='newsitemtranslation',
            options={'default_permissions': (), 'managed': True, 'verbose_name': 'news item Translation'},
        ),
        migrations.AlterModelManagers(
            name='newsitem',
            managers=[
                ('current', django.db.models.manager.Manager()),
            ],
        ),
        migrations.AlterField(
            model_name='newsitemtranslation',
            name='language_code',
            field=models.CharField(db_index=True, max_length=15, verbose_name='Language'),
        ),
        migrations.AlterField(
            model_name='newsitemtranslation',
            name='master',
            field=parler.fields.TranslationsForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='news.NewsItem'),
        ),
    ]

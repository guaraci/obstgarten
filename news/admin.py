from django.contrib import admin
from django.db import models
from django.utils.html import strip_tags

from parler.admin import TranslatableAdmin
from tinymce.widgets import TinyMCE

from .models import NewsItem


class NewsItemAdmin(TranslatableAdmin):
    class Media:
        css = {
             'all': ('css/tinymce-admin.css',)
        }

    list_display = ['content_extract', 'pub_date']
    ordering = ['-pub_date']
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(mce_attrs={
            'theme_advanced_buttons1' : "bold,italic,underline,separator,undo,redo,link,unlink",
            'entity_encoding' : "raw",
        })},
    }

    def content_extract(self, obj):
        return strip_tags(obj.content)[:60] + '…'


admin.site.register(NewsItem, NewsItemAdmin)

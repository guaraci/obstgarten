"""
This Django shell script helps tranforming Swiss municipalities data from
swissBOUNDARIES3D into locations.Location model instances.
https://shop.swisstopo.admin.ch/de/products/landscape/boundaries3D
"""
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import MultiPolygon, Point, WKTWriter

from locations.models import Location


path = "/home/claude/Téléchargements/swissBOUNDARIES3D/BOUNDARIES_2017/DATEN/swissBOUNDARIES3D/SHAPEFILE_LV95_LN02/swissBOUNDARIES3D_1_3_TLM_HOHEITSGEBIET.shp"

ds = DataSource(path)
layer = ds[0]

municipalities = {}
for feat in layer:
    if feat.get('OBJEKTART') != "Gemeindegebiet":
        continue
    name = feat.get('NAME')
    if name in municipalities:
        municipalities[name]['geoms'].append(feat.geom.geos)
    else:
        municipalities[name] = {'bfs_num': feat.get('BFS_NUMMER'), 'geoms': [feat.geom.geos]}

# Calculate centers
for name, struct in municipalities.items():
    if len(struct['geoms']) > 1:
        municipalities[name]['center'] = MultiPolygon(struct['geoms']).centroid
    else:
        municipalities[name]['center'] = struct['geoms'][0].centroid

Location.objects.bulk_create([
    # Round to meter, no need for better precision
    Location(name=name, center=Point(int(struct['center'].x), int(struct['center'].y)))
    for name, struct in municipalities.items()
])

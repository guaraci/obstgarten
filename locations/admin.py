from django.contrib import admin
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.forms import OSMWidget

from .models import Location


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    formfield_overrides = {
        gis_models.PointField: {'widget': OSMWidget},
    }
    ordering = ('name',)
    search_fields = ('name',)

from django.contrib.gis.db.models import PointField
from django.db import models

APP_SRID = 2056  # Not to be changed lightly, as this would need migrations


class Location(models.Model):
    name = models.CharField(max_length=150, unique=True)
    center = PointField(srid=APP_SRID)

    def __str__(self):
        return self.name

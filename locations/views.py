from django.http import JsonResponse
from django.views.generic import View

from .models import Location


class LocationSearchView(View):
    """
    View called from the location name autocomplete widget.
    Return the first 15 results in a jQuery-UI autocomplete-compliant format.
    """
    def get(self, request, *args, **kwargs):
        if not 'term' in request.GET:
            return JsonResponse([], safe=False)
        results = [
            loc.name
            for loc in Location.objects.filter(name__icontains=request.GET['term']).order_by('name')[:15]
        ]
        return JsonResponse(results, safe=False)

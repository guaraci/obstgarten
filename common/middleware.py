from django.middleware.locale import LocaleMiddleware
from django.utils import translation


class UserLocaleMiddleware(LocaleMiddleware):
    def process_request(self, request):
        if request.user.is_authenticated and request.user.language:
            translation.activate(request.user.language)
            request.LANGUAGE_CODE = translation.get_language()
        else:
            super().process_request(request)

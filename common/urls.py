from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path
from django.utils.translation import gettext_lazy as _

from accounts import views as accounts_views
from accounts.forms import PasswordResetForm
from locations import views as loc_views
from news import views as news_views
from orchard import views

urlpatterns = [
    path('login/', accounts_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('accounts/new_user/', accounts_views.CreateAccountView.as_view(typ='user'),
        name='create_account_user'),
    path('accounts/new_owner/', accounts_views.CreateAccountView.as_view(typ='owner'),
        name='create_account_owner'),
    path('accounts/v/<token>/', accounts_views.VerifyAccountView.as_view(),
        name='account_verification'),
    path('accounts/<int:pk>/edit/', accounts_views.EditAccountView.as_view(),
        name='edit_account'),
    path('accounts/<int:pk>/delete/', accounts_views.DeleteAccountView.as_view(),
        name='delete-account'),

    # Password reset views
    path('accounts/password_reset/',
        auth_views.PasswordResetView.as_view(
            template_name='accounts/password_reset_form.html', form_class=PasswordResetForm),
        name='password_reset'),
    path('accounts/password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html'),
        name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(template_name='accounts/password_reset_confirm.html'),
        name='password_reset_confirm'),
    path('reset/done/',
        auth_views.PasswordResetCompleteView.as_view(template_name='accounts/password_reset_complete.html'),
        name='password_reset_complete'),

    path('accounts/<int:pk>/saved_search/', accounts_views.SavedSearchList.as_view(),
        name='savedsearch-list'),
    path('accounts/<int:pk>/bookings/', accounts_views.BookingsList.as_view(),
        name='bookings-list'),
    path('accounts/<int:pk>/productorders/', accounts_views.ProductOrdersList.as_view(),
        name='productorders-list'),
    path('i18n/setlang/<lang_code>/', accounts_views.set_language, name='set_language'),

    path('', views.HomeView.as_view(), name='home'),
    path('about/', views.AboutView.as_view(), name='about'),
    path('about/taxes/', views.SimpleTemplateView.as_view(template_name='taxes.html', title=_("Gebührenreglement")),
        name='taxes'),
    path('about/impressum/', views.SimpleTemplateView.as_view(template_name='impressum.html', title="Impressum"),
        name='impressum'),
    path('about/safety/', views.SimpleTemplateView.as_view(template_name='safety.html', title=_("Safety Data Sheet")),
        name='safety'),

    path('map/', views.MapView.as_view(), name='map'),
    path('offer/', views.OfferView.as_view(), name='offer'),
    path('collect/', views.CollectView.as_view(), name='collect'),
    path('collect/save_search/', views.SearchSaveView.as_view(), name='search-save'),
    path('collect/save_search/delete/', views.SearchSaveDeleteView.as_view(),
        name='search-delete'),
    path('news/', news_views.NewsListView.as_view(), name='news'),
    path('orchard/new/', views.OrchardNewView.as_view(), name='orchard_new'),
    path('orchard/<int:pk>/', views.OrchardView.as_view(), name='orchard'),
    path('orchard/<int:pk>/sendmail/', views.OrchardContactSend.as_view(),
        name='orchard-sendmail'),
    path('orchard/<int:pk>/overview/', views.OrchardOverview.as_view(),
        name='orchard-overview'),
    path('orchard/<int:pk>/edit/', views.OrchardEditView.as_view(),
        name='orchard_edit'),
    path('orchard/<int:pk>/delete/', views.OrchardDeleteView.as_view(),
        name='orchard_delete'),

    # Orchard trees
    path('orchard/<int:pk>/tree/new/', views.TreeNewView.as_view(), name='tree_new'),
    path('orchard/<int:pk>/tree/<int:pk_tree>/', views.TreeDetailsView.as_view(),
        name='tree_details'),
    path('orchard/<int:pk>/tree/<int:pk_tree>/edit/', views.TreeEditView.as_view(),
        name='tree_edit'),
    path('orchard/<int:pk>/tree/<int:pk_tree>/copymodifs/', views.TreeCopyModifsView.as_view(),
        name='tree_copymodifs'),
    path('orchard/<int:pk>/tree/<int:pk_tree>/duplicate/', views.TreeDuplicateView.as_view(),
        name='tree_dup'),
    path('orchard/<int:pk>/tree/<int:pk_tree>/delete/', views.TreeDeleteView.as_view(),
        name='tree_delete'),

    # Orchard products
    path('orchard/<int:pk>/product/new/', views.ProductNewView.as_view(), name='product_new'),
    path('orchard/<int:pk>/product/<int:pk_product>/', views.ProductDetailView.as_view(),
        name='product_detail'),
    path('orchard/<int:pk>/map_details/', views.OrchardMapDetails.as_view(),
        name='orchard-map-details'),
    path('orchard/<int:pk>/product/<int:pk_product>/edit/', views.ProductEditView.as_view(),
        name='product_edit'),
    path('orchard/<int:pk>/product/<int:pk_product>/delete/', views.ProductDeleteView.as_view(),
        name='product_delete'),
    path('orchard/<int:pk>/productorder/<int:pk_order>/', views.ProductOrderDetailView.as_view(),
        name='productorder_detail'),
    path('products/', views.ProductListView.as_view(), name='product_list'),
    path('my_products/', views.ProductOwnView.as_view(), name='my-products'),
    path('my_orchards/', views.OrchardOwnView.as_view(), name='my-orchards'),

    path('tree/<int:pk>/map_details/', views.TreeMapDetails.as_view(),
        name='tree-map-details'),
    path('tree/<int:pk>/<int:year>/book/', views.TreeBookingView.as_view(),
        name='tree-book'),
    path('booking/delete/', views.BookingDeleteView.as_view(),
        name='booking-delete'),

    path('location_search/', loc_views.LocationSearchView.as_view(), name='location-search'),

    path('admin/', admin.site.urls),
    path('tinymce/', include('tinymce.urls')),
]

if settings.DEBUG:
    # Should be an Apache alias in production
    urlpatterns.extend(
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    )

"""
WSGI config for obstgarten project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/stable/howto/deployment/wsgi/
"""

import os

UPGRADING = False

def upgrade_in_progress(environ, start_response):
    PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    upgrade_file = os.path.join(PROJECT_ROOT, 'templates', '503.html')
    if os.path.exists(upgrade_file):
        response_headers = [('Content-type','text/html')]
        response = open(upgrade_file, mode='rb').read()
    else:
        response_headers = [('Content-type','text/plain')]
        response = b"This site is in maintenance mode, please come back in some minutes."

    if environ['REQUEST_METHOD'] == 'GET':
        status = '200 OK'
    else:
        status = '403 Forbidden'
    start_response(status, response_headers)
    return [response]

if UPGRADING:
    application = upgrade_in_progress
else:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "common.settings")

    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()

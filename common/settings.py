"""
Django settings for obstgarten project.
"""

import os
from django.utils.translation import gettext_lazy as _

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

DEBUG = False

ALLOWED_HOSTS = [
    'localhost', '127.0.0.1',
    'www.meinobstgarten.ch', 'meinobstgarten.ch',
    'www.monverger.ch', 'monverger.ch',
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.gis',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 3rd party
    'tinymce',
    'parler',
    'colorful',
    'sorl.thumbnail',
    # Our apps
    'accounts',
    'locations',
    'news',
    'orchard.apps.OrchardConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'common.middleware.UserLocaleMiddleware',  # Uses request.user
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'common.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'builtins': ['django.templatetags.i18n', 'django.templatetags.static'],
        },
    },
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'NAME': 'django-text',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'OPTIONS': {
            'autoescape': False,
            'builtins': ['django.templatetags.i18n'],
        },
    },
]

WSGI_APPLICATION = 'common.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'obstgarten',
    }
}
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {'min_length': 6},
    },
]
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = 'home'
LOGOUT_REDIRECT_URL = 'home'

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'de'
LANGUAGES = [
    ('de', _('German')),
    ('fr', _('French')),
    ('en', _('English')),
    #('pt', _('Portuguese')),
]
LOCALE_PATHS = [os.path.join(BASE_DIR, 'translations')]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },
    "staticfiles": {
        "BACKEND": "django.contrib.staticfiles.storage.ManifestStaticFilesStorage",
    },
}

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

AUTH_USER_MODEL = 'accounts.User'

THUMBNAIL_PRESERVE_FORMAT = True

PARLER_LANGUAGES = {
    None: (
        {'code': 'de'},
        {'code': 'en'},
        {'code': 'fr'},
        #{'code': 'pt',},
    ),
    'default': {
        'fallbacks': ['de'],          # defaults to PARLER_DEFAULT_LANGUAGE_CODE
        'hide_untranslated': False,   # the default; let .active_translations() return fallbacks too.
    }
}

# Default config, updated in orchard/apps.py to be able to access CSS static file
# See https://www.tinymce.com/docs-3x/reference/buttons/ for the button reference
TINYMCE_DEFAULT_CONFIG = {
    'theme_advanced_buttons1': "bold,italic,underline,strikethrough,separator,link,unlink,separator,"
                                "undo,redo,separator,cleanup,separator,bullist",
    'theme_advanced_buttons2': "",
    'theme_advanced_buttons3': "",
    'theme_advanced_toolbar_location': "bottom",
    'theme_advanced_toolbar_align': "center",
    'menubar': False,
    'relative_urls': False,
    'paste_as_text': True,
    'plugins': "autoresize, paste",
}

SITE_MANAGER = 'Raphael Häner'
SITE_EMAIL = 'info@meinobstgarten.ch'
SMTP_CONNECTIONS = {}

MAILCHIMP_APIUSER = ''
MAILCHIMP_APIKEY = ''
MAILCHIMP_LISTID = ''

from .local_settings import *

import json

from django import template
from django.forms import (CheckboxInput, Textarea, RadioSelect, Select, CheckboxSelectMultiple, HiddenInput)
from django.template.defaultfilters import stringfilter
from django.templatetags.static import static
from django.utils.safestring import mark_safe
from django.utils.text import Truncator
from django.utils.translation import gettext as _
from tinymce.widgets import TinyMCE
from orchard.forms import AutoCompleteWidget

register = template.Library()

from orchard.views import MAP_SRID

@register.filter
def is_hidden(field):
    return isinstance(field.field.widget, HiddenInput)


@register.filter
def is_autocomplete(field):
    return isinstance(field.field.widget, AutoCompleteWidget)


@register.filter
def is_multiple_checkbox(field):
    return isinstance(field.field.widget, CheckboxSelectMultiple)


@register.filter
def is_select(field):
    return isinstance(field.field.widget, Select)


@register.filter
def is_checkbox(field):
    return isinstance(field.field.widget, CheckboxInput)


@register.filter
def is_textarea(field):
    return isinstance(field.field.widget, Textarea)


@register.filter
def is_tinymce(field):
    return isinstance(field.field.widget, TinyMCE)


@register.filter
def is_radio(field):
    return isinstance(field.field.widget, RadioSelect)


@register.filter(is_safe=True)
def strip_colon(label):
    return label.replace(':<', '<')


@register.filter
def label_tag(field, strip=False):
    label = field.label_tag(attrs={'class': 'input-group__label'})
    if strip:
        label = label.replace(':<', '<')
    return mark_safe(label)


@register.filter(is_safe=True)
@stringfilter
def truncatewords_readmore(value, length):
    return Truncator(value).words(
        int(length), html=True,
        truncate=' … <a href="" class="readmore">%s</a>' % _("Read more")
    )


@register.filter
def as_geojson(obj):
    if not obj:
        return '{}'
    geo_struct = obj.geojson(target_srid=MAP_SRID)
    '''
    if obj.point.srid != 4326:
        geo_obj['crs'] = {
            'type': 'name',
            'properties': {'name': 'urn:x-ogc:def:crs:EPSG:%s' % MAP_SRID},
        }
    '''
    return mark_safe(json.dumps(geo_struct))


@register.filter
def as_wkt(obj):
    if obj.point.srid != MAP_SRID:
        obj.point.transform(MAP_SRID)
    return mark_safe(obj.point.wkt)

@register.filter
def req_level_img(tree):
    return static('img/tree_%s.png' % tree.req_level)


@register.filter
def pick_date_string(tree, year):
    return tree.pick_date_string(year)


@register.filter
def date_tuple_to_string(tpl):
    return "{} – {}".format(tpl[0].strftime('%d.%m'), tpl[1].strftime('%d.%m.%Y')) if tpl else ''

@register.filter
def pluck(queryset, field):
    return [getattr(item, field) for item in queryset]

import datetime
from collections import OrderedDict
from urllib.parse import urlencode

from django import forms
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.geos import MultiPoint
from django.contrib.postgres.fields import ArrayField
from django.contrib.staticfiles.storage import staticfiles_storage
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from colorful.fields import RGBColorField
from parler.models import TranslatableModel, TranslatedFields
from accounts.models import User
from locations.models import Location

APP_SRID = 2056  # Not to be changed lightly, as this would need migrations

YEAR_PERIOD_CHOICES = (
    ('01.01', _("Start of January")),
    ('01.15', _("Middle of January")),
    ('01.31', _("End of January")),
    ('02.01', _("Start of February")),
    ('02.15', _("Middle of February")),
    ('02.28', _("End of February")),
    ('03.01', _("Start of March")),
    ('03.15', _("Middle of March")),
    ('03.31', _("End of March")),
    ('04.01', _("Start of April")),
    ('04.15', _("Middle of April")),
    ('04.30', _("End of April")),
    ('05.01', _("Start of May")),
    ('05.15', _("Middle of May")),
    ('05.31', _("End of May")),
    ('06.01', _("Start of June")),
    ('06.15', _("Middle of June")),
    ('06.30', _("End of June")),
    ('07.01', _("Start of July")),
    ('07.15', _("Middle of July")),
    ('07.31', _("End of July")),
    ('08.01', _("Start of August")),
    ('08.15', _("Middle of August")),
    ('08.31', _("End of August")),
    ('09.01', _("Start of September")),
    ('09.15', _("Middle of September")),
    ('09.30', _("End of September")),
    ('10.01', _("Start of October")),
    ('10.15', _("Middle of October")),
    ('10.31', _("End of October")),
    ('11.01', _("Start of November")),
    ('11.15', _("Middle of November")),
    ('11.30', _("End of November")),
    ('12.01', _("Start of December")),
    ('12.15', _("Middle of December")),
    ('12.31', _("End of December")),
)

PAIEMENT_CHOICES = (
    ('adv', _('Advance paiement')),
    ('cash', _('Cash on site')),
)


class ArrayChoiceFormField(forms.TypedMultipleChoiceField):
    widget = forms.CheckboxSelectMultiple

    def __init__(self, *args, **kwargs):
        # Prevent the empty choice
        if kwargs['choices'] and kwargs['choices'][0][0] == '':
            kwargs['choices'] = kwargs['choices'][1:]
        super().__init__(*args, **kwargs)


class ChoiceArrayField(ArrayField):
    def formfield(self, **kwargs):
        # Patch from https://code.djangoproject.com/ticket/27704
        if self.base_field.choices and 'choices_form_class' not in kwargs:
            defaults = {
                'choices_form_class': ArrayChoiceFormField,
                'coerce': self.base_field.to_python,
            }
            defaults.update(kwargs)
            return self.base_field.formfield(**defaults)
        else:
            defaults = {
                'form_class': SimpleArrayField,
                'base_field': self.base_field.formfield(),
                'max_length': self.size,
            }
        defaults.update(kwargs)
        return super().formfield(**defaults)



class Photo(models.Model):
    image = models.ImageField(upload_to="photos")
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()


class VisibleQuerySet(models.QuerySet):
    """visble(user) only shows public or user-owned orchards."""
    def visible(self, user):
        return self.filter(models.Q(is_public=True) | models.Q(owner__pk=user.pk))


class Orchard(models.Model):
    owner = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(_("Name"), max_length=100)
    is_public = models.BooleanField(_("This orchard is visible to the public"), default=True,
        help_text=_("By checking this box, the orchard and its products become visible "
                    "to the public. If it's unchecked, the orchard and its products are "
                    "\"private\" and no longer publicly visible."),
    )
    municipality = models.ForeignKey(Location, verbose_name=_("Municipality"),
        on_delete=models.PROTECT)
    description = models.TextField(_("Description"))
    real_owner_name = models.CharField(_("Name"), max_length=100, blank=True)
    real_owner_addr = models.TextField(_("Address"), blank=True)
    real_owner_phone = models.CharField(_("Phone"), max_length=30, blank=True)
    real_owner_email = models.EmailField(_('email address'), blank=True)
    real_owner_paiement_info = models.TextField(_("Paiement info"), blank=True,
        help_text=_("CCP, Bank account, IBAN… to receive paiements if you charge for fruits"))
    real_owner_is_contact = models.BooleanField(default=False)
    pick_details = models.TextField(_("Pick instructions"), blank=True,
        help_text=_("Describe here details about any available ladder, its look and location, "
                    "maybe an unlocking code, available containers, whether they can be took away, etc. "
                    "This text is only visible by people having booked picking a tree in this orchard.")
    )

    photos = GenericRelation(Photo)

    objects = VisibleQuerySet.as_manager()

    def __str__(self):
        return self.name + str(_(" (private)") if not self.is_public else "")

    def get_absolute_url(self):
        return reverse('orchard', args=[self.pk])

    def geojson(self, target_srid=None):
        center = self.center
        if target_srid and center.srid != target_srid:
            center.transform(target_srid)
        return {
            'type': 'Feature',
            'id': self.pk,
            'geometry': {
                'type': 'Point',
                'coordinates': center.coords,
            },
            'properties': {
                'pk': self.pk,
                'icon': self.icon,
            },
        }

    def get_contact(self):
        if self.real_owner_is_contact:
            return {
                'name': self.real_owner_name,
                'address': self.real_owner_addr,
                'phone': self.real_owner_phone,
                'email': self.real_owner_email,
                'paiement_info': self.real_owner_paiement_info,
            }
        else:
            return {
                'name': self.owner.get_full_name(),
                'address': self.owner.get_address(),
                'phone': self.owner.phone,
                'email': self.owner.email,
                'paiement_info': self.owner.paiement_info,
            }

    @property
    def icon(self):
        return staticfiles_storage.url('img/geo-pointer.svg')

    @property
    def center(self):
        if 'trees' in getattr(self, '_prefetched_objects_cache', {}):
            tree_points = [t.point for t in self.trees.all()]
        else:
            tree_points = list(self.trees.all().values_list('point', flat=True))
        if tree_points:
            return MultiPoint(tree_points, srid=APP_SRID).centroid
        else:
            # Fallback to municipality center
            return self.municipality.center

    def can_view(self, user):
        return self.is_public or self.can_edit(user)

    def can_edit(self, user):
        return self.owner == user or user.is_superuser


class FruitCategory(TranslatableModel):
    translations = TranslatedFields(
        name = models.CharField(max_length=30),
    )
    color = RGBColorField(_("Color"), blank=True)

    def __str__(self):
        return self.name


class TreeSpecies(TranslatableModel):
    category = models.ForeignKey(FruitCategory, on_delete=models.CASCADE)
    color = RGBColorField(_("Color"), blank=True)

    translations = TranslatedFields(
        name = models.CharField(max_length=200),
        fruit = models.CharField(max_length=50),
    )

    def __str__(self):
        return self.name


REQ_CHOICES = (
    ('easy', _("Easy")),
    ('middle', _("Medium")),
    ('hard', _("Hard")),
)

USAGE_CHOICES = (
    ('dessert', _('dessert fruit')),
    ('juice', _('juice fruit')),
    ('dried', _('dried fruit')),
    ('distil', _('distillation fruit')),
    ('preserve', _('preserved fruit')),
)

class Tree(models.Model):
    AVAILABILITY_CHOICES = (
        ('none', _("Not publicly available")),
        ('whole', _("The tree can be individually booked")),
        ('picking', _("The tree is part of a self-picking offer")),
    )

    updated = models.DateTimeField()
    orchard = models.ForeignKey(Orchard, on_delete=models.CASCADE, related_name='trees')
    species = models.ForeignKey(TreeSpecies, verbose_name=_("Species"), on_delete=models.PROTECT)
    variety = models.CharField(_("Variety"), max_length=200)
    variety_descr = models.TextField(_("Variety description"), blank=True)
    availability = models.CharField(
        _("Availability"), max_length=10, choices=AVAILABILITY_CHOICES, default='none',
    )
    point = gis_models.PointField(srid=APP_SRID, verbose_name=_("Location"))
    general_descr = models.TextField(_("General description"), blank=True, help_text=_(
        "The better the tree is described and characterized, the more chances "
        "this website will be able to attract clients for your orchard."
    ))
    pick_start = models.CharField(
        _("Start of picking period"), max_length=5, choices=YEAR_PERIOD_CHOICES, blank=True
    )
    pick_end = models.CharField(
        _("End of picking period"), max_length=5, choices=YEAR_PERIOD_CHOICES, blank=True
    )
    treatment = models.TextField(_("Tree treatments"), blank=True,
        help_text=_("Treatments done or planned for the tree."))
    price = models.SmallIntegerField(
        _("Price"), blank=True, null=True,
        help_text=_("Fill price only if the tree can be individually booked.")
    )
    pay_method = ChoiceArrayField(
        models.CharField(max_length=5, choices=PAIEMENT_CHOICES),
        verbose_name=_("Paiement method(s)"), null=True,
    )

    req_level = models.CharField(_("Requirement level"), max_length=10, choices=REQ_CHOICES, blank=True)
    is_habitat = models.BooleanField(_("Habitat tree"), default=False, help_text=_(
        "A habitat tree is itself a habitat for a multitude of living animals. "
        "A fruit harvest from a ladder is not recommended. The fallen fruits can be picked up."
    ))
    habitat_note = models.TextField(_("Habitat tree comments"), blank=True)
    usages = ChoiceArrayField(
        models.CharField(max_length=10, choices=USAGE_CHOICES, blank=True),
        verbose_name=_("Fruit usage(s)"), blank=True, null=True,
    )
    photos = GenericRelation(Photo)

    def __str__(self):
        return "%s (%s)" % (self.variety, self.species.fruit)

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('tree_details', args=[self.orchard.pk, self.pk])

    def geojson(self, target_srid=None):
        point = self.point
        if target_srid and point.srid != target_srid:
            point.transform(target_srid)
        return {
            'type': 'Feature',
            'id': self.pk,
            'geometry': {
                'type': 'Point',
                'coordinates': point.coords,
            },
            'properties': {
                'pk': self.pk,
                'icon': self.icon,
                'color': self.species.color,
            },
        }

    @property
    def icon(self):
        return None  # TODO

    def pick_date_string(self, year):
        if not self.pick_start or not self.pick_end:
            return '-'
        st_mon, st_day = self.pick_start.split('.')
        end_mon, end_day = self.pick_end.split('.')
        return "%s.%s – %s.%s.%s" % (
            st_day, st_mon, end_day, end_mon, year
        )

    def price_string(self):
        if self.price is None:
            return "-"
        return "%d CHF" % self.price

    def pay_method_string(self):
        return ", ".join(str(dict(PAIEMENT_CHOICES)[meth]) for meth in self.pay_method)

    def get_usages_display(self):
        return ", ".join([str(dict(USAGE_CHOICES)[v]) for v in self.usages]) if self.usages else ''

    def fruit_quantity(self):
        try:
            return self.yearstate_set.all().order_by('-year').first().get_fruit_quant_display()
        except AttributeError:
            return '-'

    def state_for_year(self, year=None):
        if year is None:
            year = datetime.date.today().year
        if 'yearstate' in getattr(self, '_prefetched_objects_cache', []):
            # Sparing a database query
            ystate = dict([(ys.year, ys) for ys in self.yearstate_set.all()]).get(year, None)
        else:
            ystate = self.yearstate_set.filter(year=year).first()
        return ystate

    def is_passed(self, year):
        if not self.pick_end:
            return False
        return as_date(year, self.pick_end) < datetime.date.today()

    def active_booking(self, year):
        if 'booking' in getattr(self, '_prefetched_objects_cache', {}):
            # Sparing a database query
            try:
                return [
                    bk for bk in self.booking_set.all()
                    if bk.year == year and bk.status != 'cancelled'
                ][0]
            except IndexError:
                return None
        else:
            return self.booking_set.filter(year=year).exclude(status='cancelled').first()

    def booking_available(self, year):
        """
        booking is available if availability is 'whole' and pick_end is in
        the future and has no active booking.
        """
        return (
            self.availability == 'whole' and self.pick_end and
            not self.is_passed(year) and self.active_booking(year) is None
        )

    def picking_available(self, year):
        return self.availability == 'picking' and self.pick_end and not self.is_passed(year)

    @property
    def is_available_now(self):
        year = datetime.date.today().year
        state = self.state_for_year()
        return self.booking_available(year) or self.picking_available(year)

    def next_booking_year(self):
        if self.availability != 'whole':
            return None
        year = datetime.date.today().year
        if self.booking_available(year):
            return year
        if self.is_passed(year) and self.booking_available(year + 1):
            return year + 1
        return None


class YearState(models.Model):
    # Must accept the oldest year for which there is a value in db
    YEAR_CHOICES = [
        (r,r) for r in range(2016, datetime.date.today().year + 2)
    ]

    QUANTITY_CHOICES = [
        ('<10kg', '<10kg'),
        ('10-50kg', '10-50kg'),
        ('50-100kg', '50-100kg'),
        ('100-200kg', '100-200kg'),
        ('>200kg', '>200kg'),
    ]

    year = models.IntegerField(_("Year"), choices=YEAR_CHOICES)
    tree = models.ForeignKey(Tree, verbose_name=_("Tree"), on_delete=models.CASCADE)
    fruit_quant = models.CharField(_("Fruit quantity"), choices=QUANTITY_CHOICES,
                                   max_length=10, blank=True)

    def __str__(self):
        return "%s data for %s" % (self.year, self.tree)


BOOKING_STATUS_CHOICES = (
    ('planned', _("Planned")),
    ('cancelled', _("Cancelled")),
    ('done', _("Done")),
)

class Booking(models.Model):
    tree = models.ForeignKey(Tree, on_delete=models.PROTECT)
    year = models.IntegerField(_("Year"))

    user = models.ForeignKey(User, on_delete=models.PROTECT)
    status = models.CharField(max_length=12, choices=BOOKING_STATUS_CHOICES, default='planned')

    def __str__(self):
        return "Booking (%s) by %s for tree %s in orchard %s" % (
            self.status, self.user, self.tree, self.tree.orchard
        )

    def can_delete(self):
        """
        It should not be possible to delete passed bookings (accounting/stats purposes).
        """
        if not self.tree.pick_end:
            return True
        if self.status == 'done' or self.year < timezone.now().date().year:
            return False
        return True

    def archive(self, reason):
        BookingArchived.archive_booking(self, reason)
        self.delete()


class BookingArchived(models.Model):
    tree = models.JSONField()
    year = models.IntegerField(_("Year"))
    user = models.JSONField()
    status = models.CharField(max_length=12, choices=BOOKING_STATUS_CHOICES)
    archived_reason = models.TextField()
    archived_date = models.DateTimeField()

    @classmethod
    def archive_booking(cls, booking, reason):
        orchard = booking.tree.orchard
        BookingArchived.objects.create(
            user=as_json(booking.user, fields=('email', 'pcode')),
            tree={
                'orchard': {
                    'pk': orchard.pk,
                    'owner': as_json(orchard.owner, fields=('email', 'pcode')),
                    'name': orchard.name,
                    'municipality': {
                        'pk': orchard.municipality.pk,
                        'name': orchard.municipality.name,
                    },
                },
                'variety': booking.tree.variety,
                'species': booking.tree.species.name if booking.tree.species else None,
            },
            year=booking.year,
            status=booking.status,
            archived_reason=reason,
            archived_date=timezone.now(),
        )


INTERVAL_CHOICES = (
    ('', _("Never")),
    ('daily', _("Daily")),
    ('weekly', _("Weekly")),
    ('monthly', _("Monthly")),
)

class SavedSearch(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(_("Name"), max_length=200)
    interval = models.CharField(_("Notification interval"), choices=INTERVAL_CHOICES,
        max_length=12, blank=True)
    form_data = models.TextField()
    last_sent = models.DateTimeField(null=True, blank=True)
    last_tree_list = models.TextField(blank=True)

    def __str__(self):
        return self.name

    def url(self):
        params = eval(self.form_data)
        # Sorting mainly for test purposes
        params = OrderedDict((key, params[key]) for key in sorted(params.keys()))
        return reverse('collect') + '?' + urlencode(params)

    def interval_past(self):
        """Return True if interval from last_sent is over self.interval."""
        if not self.interval:
            return False
        if not self.last_sent:
            return True
        time_diff = timezone.now() - self.last_sent
        if self.interval == 'daily' and time_diff.total_seconds() > (60 * 60 * 23):
            # testing for less than one day, so as a daily mail is sent even if
            # the daily script is run with some delays.
            return True
        elif self.interval == 'weekly' and time_diff.days >= 6:
            return True
        elif self.interval == 'monthly' and time_diff.days >= 30:
            return True
        return False

    def new_trees(self):
        """Return trees found from last_sent date"""
        from .forms import SearchForm

        form = SearchForm(data={**eval(self.form_data), 'only_avail': 'on'})
        if form.is_valid():
            trees = form.search()[0]
            if self.last_tree_list:
                last_pks = self.last_tree_list.split(',')
                if isinstance(trees, list):
                    return [tree for tree in trees if str(tree.pk) not in last_pks]
                else:
                    return trees.exclude(pk__in=last_pks)
            else:
                return trees
        return []


class Product(models.Model):
    PRODTYPE_CHOICES = (
        ('self', _("Fruits to self pick")),
        ('picked', _("Picked fruits")),
        ('prod', _("Fruit products")),
    )

    orchard = models.ForeignKey(
        Orchard, on_delete=models.CASCADE, verbose_name=_("Orchard"),
        related_name='products'
    )
    title = models.CharField(_("Title"), max_length=250)
    typ = models.CharField(_("Offer type"), max_length=10, choices=PRODTYPE_CHOICES, default='prod')
    description = models.TextField(_("Description"), blank=True)
    fruit_species = models.ManyToManyField(
        TreeSpecies, blank=True, verbose_name=_("Fruits"), related_name='products'
    )
    availability = models.BooleanField(_("Availability"), default=True,
        help_text=_("Once your product is sold out, you can simply uncheck this box. "
                    "It can not be ordered anymore."))
    price = models.DecimalField(
        _("Price"), max_digits=6, decimal_places=2, null=True, blank=True,
        help_text=_("Note the meinobstgarten.ch platform may charge you up to 7% of the price.")
    )
    unit = models.CharField(_("Unit"), max_length=15, blank=True)
    sending_costs = models.DecimalField(
        _("Shipping costs"), max_digits=6, decimal_places=2, null=True, blank=True,
        help_text=_("If the product cannot be shipped, specify it in the product description.")
    )

    photos = GenericRelation(Photo)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('product_detail', args=[self.orchard.pk, self.pk])

    def target_trees(self):
        if self.typ == 'self':
            # Try to leverage prefetched objects
            if 'trees' in getattr(self.orchard, '_prefetched_objects_cache', []):
                return [
                    t for t in self.orchard._prefetched_objects_cache['trees']
                    if t.availability == 'picking' and t.species_id in [fs.pk for fs in self._prefetched_objects_cache['fruit_species']]
                ]
            else:
                return self.orchard.trees.filter(
                    species__in=self.fruit_species.all(),
                    availability='picking'
                )
        return self.orchard.trees.none()

    def next_availability_dates(self):
        trees = self.target_trees()
        if isinstance(trees, list):
            pick_limits = [(t.pick_start, t.pick_end) for t in trees if t.pick_start]
        else:
            pick_limits = trees.exclude(pick_start='').values_list('pick_start', 'pick_end')
        if not pick_limits:
            return None
        start, end = '12.31', '01.01'
        for start_, end_ in pick_limits:
            if start_ < start:
                start = start_
            if end_ > end:
                end = end_
        this_year = datetime.date.today().year
        if datetime.date.today() > as_date(this_year, end) + datetime.timedelta(days=10):
            start_date, end_date = as_date(this_year + 1, start), as_date(this_year + 1, end)
        else:
            start_date, end_date = as_date(this_year, start), as_date(this_year, end)
        return (start_date, end_date)

    def get_confirmation_template(self):
        if self.typ == 'self':
            return 'email/product_selfpick_order_confirm.txt'
        else:
            return 'email/product_order_confirm.txt'


STATUS_CHOICES = (
    ('ordered', _("Ordered")),
    ('confirmed', _("Confirmed")),
    ('payed', _("Payed")),
    ('sent', _("Sent")),
    ('cancelled', _("Cancelled")),
)


class ProductOrder(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField(_("Quantity"), default=1)
    total_price = models.DecimalField(_("Total price"), max_digits=6, decimal_places=2)
    order_date = models.DateTimeField(_("Order date"))
    confirm_date = models.DateTimeField(null=True, blank=True)
    send_date = models.DateTimeField(null=True, blank=True)
    cancel_date = models.DateTimeField(null=True, blank=True)
    cancel_reason = models.TextField(blank=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='ordered')

    def __str__(self):
        return "{user} ordered quantity {quant} of product {prod} on {date}".format(
            user=self.user, quant=self.quantity, prod=self.product, date=self.order_date)

    def get_absolute_url(self):
        return reverse('productorder_detail', args=[self.product.orchard.pk, self.pk])

    @property
    def order_id(self):
        return 10000 + self.pk

    def can_view(self, user):
        return user == self.user or user == self.product.orchard.owner or user.is_superuser

    def archive(self, reason):
        ProductOrderArchived.archive_order(self, reason)
        self.delete()


class ProductOrderArchived(models.Model):
    user = models.JSONField()
    product = models.JSONField()
    quantity = models.PositiveIntegerField(_("Quantity"))
    total_price = models.DecimalField(_("Total price"), max_digits=6, decimal_places=2)
    order_date = models.DateTimeField(_("Order date"))
    confirm_date = models.DateTimeField(null=True, blank=True)
    send_date = models.DateTimeField(null=True, blank=True)
    cancel_date = models.DateTimeField(null=True, blank=True)
    cancel_reason = models.TextField(blank=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    archived_reason = models.TextField()
    archived_date = models.DateTimeField()

    @classmethod
    def archive_order(cls, order, reason):
        orchard = order.product.orchard
        ProductOrderArchived.objects.create(
            user=as_json(order.user, fields=('email', 'pcode')),
            product={
                'orchard': {
                    'pk': orchard.pk,
                    'owner': as_json(orchard.owner, fields=('email', 'pcode')),
                    'name': orchard.name,
                    'municipality': {
                        'pk': orchard.municipality.pk,
                        'name': orchard.municipality.name,
                    },
                },
                'title': order.product.title, 'typ': order.product.typ,
                'fruit_species': [
                    {'pk': species.pk, 'name':species.name}
                    for species in order.product.fruit_species.all()
                ],
            },
            quantity=order.quantity,
            total_price=order.total_price,
            order_date=order.order_date,
            confirm_date=order.confirm_date,
            send_date=order.send_date,
            cancel_date=order.cancel_date,
            cancel_reason=order.cancel_reason,
            status=order.status,
            archived_reason=reason,
            archived_date=timezone.now(),
        )


def as_date(year, pick_string):
    """Return date object from (2016, '04.31')."""
    return datetime.date(year, *[int(s) for s in pick_string.split('.')])


def as_json(obj, fields):
    """Return a Python structure that can be easily JSONified."""
    return {f: getattr(obj, f) for f in fields}

import json
from datetime import date, timedelta, datetime

from django import forms
from django.forms import widgets
from django.forms.models import ModelChoiceIterator
from django.contrib.contenttypes.forms import generic_inlineformset_factory
from django.contrib.gis.forms import BaseGeometryWidget
from django.contrib.gis.measure import Distance
from django.db.models import Count, Q
from django.db.models.functions import Length
from django.forms.models import inlineformset_factory
from django.templatetags.static import static
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.utils.translation import pgettext_lazy, gettext_lazy as _

from sorl.thumbnail.admin.current import AdminImageWidget
from tinymce.widgets import TinyMCE
from accounts.forms import LabelSortedModelMultipleChoiceField

from locations.models import Location
from .models import (
    Orchard, Photo, Product, ProductOrder, SavedSearch, Tree, TreeSpecies,
    YearState,
)


class LabelOrderedIterator(ModelChoiceIterator):
    """Sort choices by label, except the first choice."""
    def __iter__(self):
        choices = list(super().__iter__())
        yield choices[0]
        for choice in sorted(choices[1:], key=lambda ch: ch[1].lower()):
            yield choice


class GeoAdminWidget(BaseGeometryWidget):
    template_name = 'geoadmin_widget.html'
    map_srid = 21781
    default_lon = 620000
    default_lat = 215000

    def __init__(self, attrs=None):
        super().__init__(attrs=attrs)
        self.attrs.update({'default_lon': self.default_lon, 'default_lat': self.default_lat })


class AutoCompleteWidget(forms.TextInput):
    def format_value(self, value):
        if value:
            # Otherwise, the PK appears in the input
            try:
                return Location.objects.get(pk=value)
            except (Location.DoesNotExist, ValueError):
                pass
        return super().format_value(value)


class BSClassMixin:
    def __init__(self, user=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            if isinstance(field.widget, (widgets.Textarea, widgets.Input)):
                if 'class' in field.widget.attrs:
                    field.widget.attrs['class'] += ' input-group__field'
                else:
                    field.widget.attrs['class'] = 'input-group__field'


class OrchardForm(BSClassMixin, forms.ModelForm):
    # Autocomplete on client-side
    municipality = forms.CharField(label=_("Municipality"), widget=AutoCompleteWidget())
    i_am_owner = forms.BooleanField(widget=forms.RadioSelect(choices=[
        (True, _("I'm the owner of this orchard")),
        (False, _("The owner of this orchard is someone else")),
    ]), required=False)

    class Meta:
        model = Orchard
        fields = [
            'name', 'municipality', 'is_public', 'description', 'pick_details',
            'i_am_owner', 'real_owner_name', 'real_owner_addr', 'real_owner_phone',
            'real_owner_email', 'real_owner_paiement_info', 'real_owner_is_contact',
        ]
        widgets = {
            'description': TinyMCE,
            'pick_details': TinyMCE,
        }
        labels = {
            'real_owner_is_contact': _("Use the above information as contact data for site users"),
        }

    def __init__(self, user=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.instance.owner_id:
            self.instance.owner = user
        self.fields['i_am_owner'].initial = self.instance.real_owner_name == ''

    def clean_municipality(self):
        try:
            return Location.objects.get(name=self.cleaned_data['municipality'])
        except Location.DoesNotExist:
            raise forms.ValidationError(
                _("'%s' is not a recognized Swiss municipality." % self.cleaned_data['municipality'])
            )


class YearStateForm(forms.ModelForm):
    class Meta:
        model = YearState
        fields = '__all__'
        widgets = {
            'year': forms.HiddenInput,
        }


class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = '__all__'
        widgets = {
            'image': AdminImageWidget,
        }


class ReqLevelRadioSelect(forms.RadioSelect):
    def create_option(self, *args, **kwargs):
        option = super().create_option(*args, **kwargs)
        if option['value']:
            option['label'] = format_html(
                '{}<br><img src="{}">', option['label'], static('img/tree_%s.png' % option['value'])
            )
        return option


class AvailabilityRadioSelect(forms.RadioSelect):
    option_template_name = 'avail_option.html'


class TreeForm(BSClassMixin, forms.ModelForm):
    class Meta:
        model = Tree
        exclude = ['updated']
        widgets = {
            'orchard': forms.HiddenInput,
            'availability': AvailabilityRadioSelect,
            'point': GeoAdminWidget,
            'variety_descr': TinyMCE,
            'general_descr': TinyMCE,
            'pay_method': forms.CheckboxSelectMultiple,
            'req_level': ReqLevelRadioSelect,
        }

    # list of field names which can be copied over to similar trees.
    allowed_copy_fields = (
        'variety_descr', 'pick_start', 'pick_end', 'treatment', 'price', 'pay_method',
        'req_level', 'usages',
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        orchard = kwargs['initial']['orchard'] if 'orchard' in kwargs['initial'] else None
        center = None
        if not self.instance.pk and orchard:
            if orchard.trees.exists():
                # Set initial position as the center of other trees of the orchard
                center = orchard.center
            elif orchard.municipality:
                center = orchard.municipality.center
            if center:
                center.transform(GeoAdminWidget.map_srid)
                self.fields['point'].widget.attrs.update(
                    {'default_lon': center.coords[0], 'default_lat': center.coords[1] }
                )
        # Zoom more if editing a tree or if a center is defined
        if self.instance.pk or center:
            self.fields['point'].widget.attrs['resolution'] = 0.5
        # Remove the initial empty choice
        self.fields['pay_method'].choices = [c for c in self.fields['pay_method'].choices if c[0]]
        self.fields['pay_method'].required = False
        self.fields['price'].label += ' (CHF)'

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('price') and not cleaned_data.get('pay_method'):
            raise forms.ValidationError(
                _("If you set a price, your must choose at least one paiement method.")
            )

    def save(self, *args, **kwargs):
        tree = super().save(*args, **kwargs)
        if tree.availability == 'picking':
            # Check if a self-picking product exists, and create one if not
            if not tree.orchard.products.filter(typ='self', fruit_species=tree.species).exists():
                prod = Product.objects.create(
                    orchard=tree.orchard, typ='self',
                    title=_("%s self-picking") % tree.species,
                )
                prod.fruit_species.add(tree.species)
                tree._message = format_html(_(
                    'A new self-picking product has been automatically created. '
                    'Please <a href="{}">edit its details</a> now!'
                ), reverse('product_edit', args=[tree.orchard.pk, prod.pk]))
        return tree


class TreeCopyModifsForm(forms.Form):
    next = forms.CharField(widget=forms.HiddenInput)

    def __init__(self, *args, tree=None, fields=(), **kwargs):
        super().__init__(*args, **kwargs)
        # build a checkbox boolean for each field in fields
        self.tree = tree
        self.field_names = fields
        for field in fields:
            self.fields[field] = forms.BooleanField(
                label=tree._meta.get_field(field).verbose_name,
                initial=True, required=False
            )

    def get_values(self):
        for field in self.field_names:
            if hasattr(self.tree, 'get_%s_display' % field):
                value = getattr(self.tree, 'get_%s_display' % field)()
            else:
                value = getattr(self.tree, field)
            yield self[field], value or '-'


YearStateFormSet = inlineformset_factory(Tree, YearState, form=YearStateForm, extra=1)
PhotosFormSet = generic_inlineformset_factory(Photo, form=PhotoForm, extra=1)


class ProductForm(BSClassMixin, forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'
        field_classes = {
            'fruit_species': LabelSortedModelMultipleChoiceField,
        }
        widgets = {
            'orchard': forms.HiddenInput,
            'description': TinyMCE,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['price'].widget.attrs['placeholder'] = '0.00'
        self.fields['price'].widget.attrs['min'] = '0'
        self.fields['sending_costs'].widget.attrs['min'] = '0'

        if self.instance.pk is None:
            # Only when adding a new product
            self.fields['confirm_conditions'] = forms.BooleanField(
                label=format_html(_(
                    'I accept <a href="%s" target="_blank">this Website\'s fee regulations</a>.'
                ), reverse('taxes'))
            )


class ProductOrderForm(BSClassMixin, forms.ModelForm):
    class Meta:
        model = ProductOrder
        fields = ('user', 'product', 'quantity')
        widgets = {
            'user': forms.HiddenInput,
            'product': forms.HiddenInput,
        }

    def save(self, **kwargs):
        self.instance.total_price = (
            (self.instance.product.price or 0) * self.instance.quantity
        ) + (self.instance.product.sending_costs or 0)
        self.instance.order_date = timezone.now()
        self.instance.status = 'ordered'
        return super().save(**kwargs)


class ProductOrderConfirmForm(BSClassMixin, forms.Form):
    step = forms.CharField(max_length=10, initial='confirm', widget=forms.HiddenInput)
    remark = forms.CharField(
        label=_("Remark (sent to the client, if filled)"),
        max_length=500, required=False, widget=forms.Textarea
    )


class FruitChoiceField(forms.ModelChoiceField):
    iterator = LabelOrderedIterator

    def label_from_instance(self, obj):
        return obj.fruit


class OrchardContactForm(BSClassMixin, forms.Form):
    message = forms.CharField(label=_("Message"), widget=forms.Textarea, required=True)


class SearchForm(BSClassMixin, forms.Form):
    category = FruitChoiceField(
        label=_("Fruit"), queryset=None, empty_label=pgettext_lazy("Fruits", "All"), required=False)
    from_loc = forms.CharField(label=_("From"), required=False)
    radius = forms.IntegerField(label=_("within a radius of"), min_value=0, required=False)
    only_avail = forms.BooleanField(
        label=_("Only return trees with picking opportunities this year"),
        initial=True,
        required=False,
    )
    only_pickable = forms.BooleanField(
        label=_("Only return trees that are pickable in this period"),
        initial=True,
        required=False,
    )
    _typ = (('tree', _('Trees')),) + Product.PRODTYPE_CHOICES
    typ = forms.MultipleChoiceField(label=_('Product type'), widget=forms.CheckboxSelectMultiple, choices=_typ, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Set at runtime because ordering depends on current language
        self.fields['category'].queryset = TreeSpecies.objects.active_translations(
            ).annotate(num_tree=Count('tree')
            ).filter(num_tree__gt=0).distinct()

    def clean_from_loc(self):
        if self.cleaned_data.get('from_loc'):
            try:
                return Location.objects.get(name__iexact=self.cleaned_data['from_loc'])
            except Location.DoesNotExist:
                raise forms.ValidationError(
                    _("'%s' is not a recognized Swiss municipality.") % self.cleaned_data['from_loc']
                )

    def clean(self):
        if not any([
                self.cleaned_data.get('category'), self.cleaned_data.get('only_avail'), self.cleaned_data.get('typ'),
                (self.cleaned_data.get('from_loc') and self.cleaned_data.get('radius'))]):
            raise forms.ValidationError(_("No valid search criteria"))
        return super().clean()

    def search(self):
        trees = Tree.objects.filter(orchard__is_public=True).exclude(availability='none')
        products = Product.objects.filter(orchard__is_public=True)

        if self.cleaned_data['category']:
            trees = trees.filter(species=self.cleaned_data['category'])
            products = products.filter(fruit_species=self.cleaned_data['category'])

        if self.cleaned_data['from_loc'] and self.cleaned_data['radius']:
            trees = trees.filter(
                point__dwithin=(self.cleaned_data['from_loc'].center, Distance(km=self.cleaned_data['radius'])),
            )
            products = products.filter(
                orchard__municipality__center__dwithin=(self.cleaned_data['from_loc'].center, Distance(km=self.cleaned_data['radius'])),
            )

        if self.cleaned_data['only_avail']:
            today = date.today()
            trees = trees.exclude(pick_end__isnull=True).filter(
                pick_end__gte='{:02}.{:02}'.format(today.month, today.day)
            )

        if self.cleaned_data['only_pickable']:
            now = datetime.now().strftime("%m.%d")
            trees = trees.filter(pick_start__lte=now, pick_end__gte=now)

        if self.cleaned_data['typ']:
            if not 'tree' in self.cleaned_data['typ']:
                trees = Tree.objects.none()
            products = products.filter(typ__in=self.cleaned_data['typ'])

        return trees, products

    def name_from_data(self):
        """Compute a name for SavedSearch from this form cleaned data."""
        return "{cat} - {loc} ({km} km)".format(
            cat=self.cleaned_data['category'].fruit if self.cleaned_data['category'] else _("All fruits"),
            loc=self.cleaned_data['from_loc'],
            km=self.cleaned_data['radius']
        )


class SearchSaveForm(BSClassMixin, forms.ModelForm):
    class Meta:
        model = SavedSearch
        fields = ('name', 'interval', 'form_data')
        widgets = {
            'form_data': forms.HiddenInput
        }

    def clean_form_data(self):
        data = self.cleaned_data.get('form_data')
        if data:
            return {k: v for k, v in [
                couple.split('=', maxsplit=1) for couple in data.split('&')]
            }
        return data

    def save(self, user, **kwargs):
        self.instance.user = user
        return super().save(**kwargs)

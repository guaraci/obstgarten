from django.core.management.commands.makemessages import Command as MakeMessageCommand


class Command(MakeMessageCommand):
    def handle(self, *args, **options):
        options['no_location'] = True
        return super().handle(*args, **options)

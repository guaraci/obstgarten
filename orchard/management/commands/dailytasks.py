from itertools import chain

from django.conf import settings
from django.core.management import BaseCommand, call_command
from django.template import loader
from django.utils import timezone
from django.utils.translation import override, gettext as _

from orchard.mail import send_mail
from orchard.models import ProductOrder, SavedSearch


class Command(BaseCommand):
    def handle(self, *args, **options):
        call_command('clearsessions')
        self.send_new_search_results()
        self.send_products_reminders()

    def send_new_search_results(self):
        """
        Based on user's SavedSearch objects, send new tree offerings since last
        notice.
        """
        for search in SavedSearch.objects.exclude(interval=''):
            # 1. determine if interval is reached
            if not search.interval_past():
                continue
            with override(search.user.language):
                # 2. determine if new trees are available
                results = search.new_trees()
                if not results:
                    continue
                # 3. compose and send the message
                hostname = _("meinobstgarten.ch")
                body = loader.render_to_string('email/new_trees.txt', {
                    'hostname': hostname,
                    'search_url': search.url(),
                    'trees': results,
                }, using='django-text')
                send_mail(
                    _('[%s] New trees available for your saved search') % hostname,
                    body,
                    settings.SITE_EMAIL,
                    [search.user.email],
                )
            # 4. update search last_* fields
            search.last_sent = timezone.now()
            search.last_tree_list = ",".join(
                str(pk) for pk in chain(search.last_tree_list.split(','), get_pks(results)) if pk
            )
            search.save()

    def send_products_reminders(self):
        """
        Unconfirmed product orders are sent to the site admin to monitor them.
        """
        pending_prod_orders = ProductOrder.objects.filter(
            status='ordered', order_date__lt=timezone.now() - timezone.timedelta(days=3)
        )
        if not pending_prod_orders.count():
            return
        hostname = _("meinobstgarten.ch")
        body = loader.render_to_string('email/pending_orders.txt', {
            'hostname': hostname,
            'pending_orders': pending_prod_orders,
        }, using='django-text')
        send_mail(
            _('[%s] Pending orders') % hostname,
            body,
            settings.SITE_EMAIL,
            [settings.SITE_EMAIL],
        )


def get_pks(results):
    if isinstance(results, list):
        return sorted([r.pk for r in results])
    else:
        return results.values_list('id', flat=True).order_by('id')

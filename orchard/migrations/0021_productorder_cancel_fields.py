from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0020_productorder'),
    ]

    operations = [
        migrations.AddField(
            model_name='productorder',
            name='cancel_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='productorder',
            name='cancel_reason',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='productorder',
            name='status',
            field=models.CharField(choices=[('ordered', 'Ordered'), ('confirmed', 'Confirmed'), ('payed', 'Payed'), ('sent', 'Sent'), ('cancelled', 'Cancelled')], default='ordered', max_length=20),
        ),
    ]

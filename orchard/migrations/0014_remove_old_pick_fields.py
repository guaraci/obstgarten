from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0013_migrate_old_pick_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='yearstate',
            name='pick_end_old',
        ),
        migrations.RemoveField(
            model_name='yearstate',
            name='pick_start_old',
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0031_product_typ'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orchard',
            name='municipality',
            field=models.ForeignKey(on_delete=models.deletion.PROTECT, to='locations.Location', verbose_name='Municipality'),
        ),
    ]

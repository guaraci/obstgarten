from django.db import migrations
from django.db.models import Count


def migrate_english(apps, schema_editor):
    Tree = apps.get_model('orchard', 'Tree')

    for tree in Tree.objects.annotate(num=Count('yearstate')).exclude(num=0):
        # get the most recent year state
        state = tree.yearstate_set.order_by('-year').first()
        # copy values to tree
        if tree.pick_start:
            tree.pick_start = '{:02}.{:02}'.format(state.pick_start.month, state.pick_start.day)
        if tree.pick_end:
            tree.pick_end = '{:02}.{:02}'.format(state.pick_end.month, state.pick_end.day)
        tree.price = state.price
        tree.pay_method = state.pay_method
        tree.save()
    

class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0033_move_fields_to_tree'),
    ]

    operations = [migrations.RunPython(migrate_english)]

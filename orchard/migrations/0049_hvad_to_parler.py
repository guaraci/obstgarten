from django.db import migrations, models
import django.db.models.deletion
import parler.fields


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0048_remove_prod_species'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fruitcategory',
            options={},
        ),
        migrations.AlterModelOptions(
            name='fruitcategorytranslation',
            options={'default_permissions': (), 'managed': True, 'verbose_name': 'fruit category Translation'},
        ),
        migrations.AlterModelOptions(
            name='treespecies',
            options={},
        ),
        migrations.AlterModelOptions(
            name='treespeciestranslation',
            options={'default_permissions': (), 'managed': True, 'verbose_name': 'tree species Translation'},
        ),
        migrations.AlterModelManagers(
            name='fruitcategory',
            managers=[
            ],
        ),
        migrations.AlterModelManagers(
            name='treespecies',
            managers=[
            ],
        ),
        migrations.AlterField(
            model_name='fruitcategorytranslation',
            name='language_code',
            field=models.CharField(db_index=True, max_length=15, verbose_name='Language'),
        ),
        migrations.AlterField(
            model_name='fruitcategorytranslation',
            name='master',
            field=parler.fields.TranslationsForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='orchard.FruitCategory'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(blank=True, decimal_places=2, help_text='Note the meinobstgarten.ch platform may charge you up to 7% of the price.', max_digits=6, null=True, verbose_name='Price'),
        ),
        migrations.AlterField(
            model_name='treespeciestranslation',
            name='language_code',
            field=models.CharField(db_index=True, max_length=15, verbose_name='Language'),
        ),
        migrations.AlterField(
            model_name='treespeciestranslation',
            name='master',
            field=parler.fields.TranslationsForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='orchard.TreeSpecies'),
        ),
    ]

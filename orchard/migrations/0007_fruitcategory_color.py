# Generated by Django 1.10.2 on 2016-10-26 08:39
import colorful.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0006_remove_name_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='fruitcategory',
            name='color',
            field=colorful.fields.RGBColorField(blank=True, verbose_name='Color'),
        ),
    ]

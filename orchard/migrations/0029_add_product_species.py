from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0028_add_tree_habitat'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='species',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='orchard.TreeSpecies', verbose_name='Fruit'),
        ),
        migrations.AlterField(
            model_name='tree',
            name='orchard',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='trees', to='orchard.Orchard'),
        ),
    ]

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orchard', '0019_product'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(default=1, verbose_name='Quantity')),
                ('total_price', models.DecimalField(decimal_places=2, max_digits=6, verbose_name='Total price')),
                ('order_date', models.DateTimeField(verbose_name='Order date')),
                ('confirm_date', models.DateTimeField(blank=True, null=True)),
                ('send_date', models.DateTimeField(blank=True, null=True)),
                ('status', models.CharField(choices=[('ordered', 'Ordered'), ('confirmed', 'Confirmed'), ('payed', 'Payed'), ('sent', 'Sent')], default='ordered', max_length=20)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='orchard.Product')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

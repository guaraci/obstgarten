from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0042_migrate_booking_tree_year'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='yearstate',
        ),
        migrations.AlterField(
            model_name='booking',
            name='tree',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='orchard.Tree'),
        ),
        migrations.AlterField(
            model_name='booking',
            name='year',
            field=models.IntegerField(verbose_name='Year'),
        ),
    ]

import colorful.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0022_treespeciestranslation_fruit'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='color',
            field=colorful.fields.RGBColorField(blank=True, verbose_name='Color'),
        ),
    ]

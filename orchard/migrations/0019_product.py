from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0018_yearstate_price_paiement'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250, verbose_name='Title')),
                ('description', models.TextField(blank=True, verbose_name='Description')),
                ('availability', models.BooleanField(default=True, help_text="Once your product is sold out, you can simply uncheck this box. It can not be ordered anymore.", verbose_name='Availability')),
                ('price', models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True, verbose_name='Price', help_text="Please specify the unit in the product description. e.g. Fr. per kg.\nNote the meinobstgarten.ch platform may charge you up to 7% of the price.")),
                ('sending_costs', models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True, verbose_name='Shipping costs', help_text='If the product cannot be shipped, specify it in the product description.')),
                ('orchard', models.ForeignKey(on_delete=models.deletion.CASCADE, related_name='products', to='orchard.Orchard', verbose_name='Orchard')),
            ],
        ),
    ]

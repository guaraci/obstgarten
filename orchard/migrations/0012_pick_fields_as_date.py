from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0011_add_real_owner_fields'),
    ]

    operations = [
        migrations.RenameField(
            model_name='yearstate',
            old_name='pick_end',
            new_name='pick_end_old',
        ),
        migrations.RenameField(
            model_name='yearstate',
            old_name='pick_start',
            new_name='pick_start_old',
        ),
        migrations.AddField(
            model_name='yearstate',
            name='pick_end',
            field=models.DateField(blank=True, null=True, verbose_name='End of picking period'),
        ),
        migrations.AddField(
            model_name='yearstate',
            name='pick_start',
            field=models.DateField(blank=True, null=True, verbose_name='Start of picking period'),
        ),
    ]

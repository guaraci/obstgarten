from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0036_yearstate_availability'),
    ]

    operations = [
        migrations.AlterField(
            model_name='yearstate',
            name='availability',
            field=models.CharField(choices=[('none', 'Not available this year'), ('whole', 'The tree can be individually booked'), ('picking', 'The tree is part of a self-picking offer')], default='none', max_length=10, verbose_name=''),
        ),
    ]

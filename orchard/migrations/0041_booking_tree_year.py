from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0040_migrate_ys_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='tree',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='orchard.Tree'),
        ),
        migrations.AddField(
            model_name='booking',
            name='year',
            field=models.IntegerField(null=True, verbose_name='Year'),
        ),
    ]

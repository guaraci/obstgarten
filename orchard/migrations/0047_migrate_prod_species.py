from django.db import migrations


def migrate_prod_species(apps, schema_editor):
    Product = apps.get_model('orchard', 'Product')
    for prod in Product.objects.exclude(species__isnull=True):
        prod.fruit_species.add(prod.species)


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0046_add_product_m2m_species'),
    ]

    operations = [migrations.RunPython(migrate_prod_species, reverse_code=migrations.RunPython.noop)]

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0001_initial'),
        ('orchard', '0023_treespecies_color'),
    ]

    operations = [
        migrations.RenameField(
            model_name='orchard',
            old_name='municipality',
            new_name='municipality_old',
        ),
        migrations.AddField(
            model_name='orchard',
            name='municipality',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.Location', verbose_name='Municipality'),
        ),
    ]

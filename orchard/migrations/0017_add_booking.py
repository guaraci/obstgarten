from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('orchard', '0016_add_savedsearch_last_fields'),
    ]

    operations = [
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('planned', 'Planned'), ('cancelled', 'Cancelled'), ('done', 'Done')], default='planned', max_length=12)),
                ('user', models.ForeignKey(on_delete=models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('yearstate', models.ForeignKey(on_delete=models.deletion.PROTECT, to='orchard.YearState')),
            ],
        ),
    ]

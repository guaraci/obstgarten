import datetime
from django.db import migrations


def migrate_pick_fields(apps, schema_editor):
    YearState = apps.get_model('orchard', 'YearState')
    for ys in YearState.objects.exclude(pick_start_old=''):
        day, month = [int(s) for s in ys.pick_start_old.split('.')]
        ys.pick_start = datetime.date(year=ys.year, month=month, day=day)
        day, month = [int(s) for s in ys.pick_end_old.split('.')]
        ys.pick_end = datetime.date(year=ys.year, month=month, day=day)
        ys.save()


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0012_pick_fields_as_date'),
    ]

    operations = [migrations.RunPython(migrate_pick_fields)]

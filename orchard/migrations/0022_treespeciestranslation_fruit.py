from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0021_productorder_cancel_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespeciestranslation',
            name='fruit',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]

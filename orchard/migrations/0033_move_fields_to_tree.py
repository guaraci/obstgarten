from django.db import migrations, models
import orchard.models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0032_orchard_municipality_mandatory'),
    ]

    operations = [
        migrations.AddField(
            model_name='tree',
            name='pay_method',
            field=orchard.models.ChoiceArrayField(base_field=models.CharField(choices=[('adv', 'Advance paiement'), ('cash', 'Cash on site')], max_length=5), null=True, size=None, verbose_name='Paiement method(s)'),
        ),
        migrations.AddField(
            model_name='tree',
            name='pick_end',
            field=models.CharField(blank=True, choices=orchard.models.YEAR_PERIOD_CHOICES, max_length=5, verbose_name='End of picking period'),
        ),
        migrations.AddField(
            model_name='tree',
            name='pick_start',
            field=models.CharField(blank=True, choices=orchard.models.YEAR_PERIOD_CHOICES, max_length=5, verbose_name='Start of picking period'),
        ),
        migrations.AddField(
            model_name='tree',
            name='price',
            field=models.SmallIntegerField(blank=True, help_text='Fill price only if the tree can be individually booked.', null=True, verbose_name='Price'),
        ),
    ]

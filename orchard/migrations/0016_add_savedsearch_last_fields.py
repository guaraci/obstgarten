from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0015_savedsearch'),
    ]

    operations = [
        migrations.AddField(
            model_name='savedsearch',
            name='last_sent',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='savedsearch',
            name='last_tree_list',
            field=models.TextField(blank=True),
        ),
    ]

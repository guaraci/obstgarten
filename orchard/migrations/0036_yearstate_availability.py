from django.db import migrations, models
import orchard.models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0035_remove_moved_yearstate_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='yearstate',
            name='availability',
            field=models.CharField(choices=[('none', 'Not available this year'), ('whole', 'The tree can be individually booked'), ('picking', 'The tree is part of a self-picking offer')], default='whole', max_length=10, verbose_name=''),
        ),
    ]

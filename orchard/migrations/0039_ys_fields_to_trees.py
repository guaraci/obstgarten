from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0038_product_unit'),
    ]

    operations = [
        migrations.AddField(
            model_name='tree',
            name='availability',
            field=models.CharField(choices=[('none', 'Not publicly available'), ('whole', 'The tree can be individually booked'), ('picking', 'The tree is part of a self-picking offer')], default='none', max_length=10, verbose_name='Availability'),
        ),
        migrations.AddField(
            model_name='tree',
            name='treatment',
            field=models.TextField(blank=True, help_text='Treatments done or planned for the tree.', verbose_name='Tree treatments'),
        ),
    ]

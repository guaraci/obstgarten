from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0025_remove_orchard_municipality_old'),
    ]

    operations = [
        migrations.AddField(
            model_name='tree',
            name='req_level',
            field=models.CharField(blank=True, choices=[('easy', 'Easy'), ('middle', 'Medium'), ('hard', 'Hard')], max_length=10, verbose_name='Requirement level'),
        ),
    ]

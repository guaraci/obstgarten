from django.db import migrations, models
import orchard.models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0029_add_product_species'),
    ]

    operations = [
        migrations.AddField(
            model_name='tree',
            name='usages',
            field=orchard.models.ChoiceArrayField(base_field=models.CharField(choices=orchard.models.USAGE_CHOICES, blank=True, max_length=10), blank=True, null=True, size=None, verbose_name='Fruit usage(s)'),
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0043_booking_delete_yearstate'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='yearstate',
            name='availability',
        ),
        migrations.RemoveField(
            model_name='yearstate',
            name='treatment',
        ),
    ]

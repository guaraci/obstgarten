from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0026_tree_req_level'),
    ]

    operations = [
        migrations.AddField(
            model_name='orchard',
            name='pick_details',
            field=models.TextField(blank=True, help_text='Describe here details about any available ladder, its look and location, maybe an unlocking code, available containers, whether they can be took away, etc. This text is only visible by people having booked picking a tree in this orchard.', verbose_name='Pick instructions'),
        ),
    ]

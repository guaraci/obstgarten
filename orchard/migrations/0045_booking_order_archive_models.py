from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0044_yearstate_remove_fields'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingArchived',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tree', models.JSONField()),
                ('user', models.JSONField()),
                ('year', models.IntegerField(verbose_name='Year')),
                ('status', models.CharField(choices=[('planned', 'Planned'), ('cancelled', 'Cancelled'), ('done', 'Done')], max_length=12)),
                ('archived_reason', models.TextField()),
                ('archived_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='ProductOrderArchived',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.JSONField()),
                ('product', models.JSONField()),
                ('quantity', models.PositiveIntegerField(verbose_name='Quantity')),
                ('total_price', models.DecimalField(decimal_places=2, max_digits=6, verbose_name='Total price')),
                ('order_date', models.DateTimeField(verbose_name='Order date')),
                ('confirm_date', models.DateTimeField(blank=True, null=True)),
                ('send_date', models.DateTimeField(blank=True, null=True)),
                ('cancel_date', models.DateTimeField(blank=True, null=True)),
                ('cancel_reason', models.TextField(blank=True)),
                ('status', models.CharField(choices=[('ordered', 'Ordered'), ('confirmed', 'Confirmed'), ('payed', 'Payed'), ('sent', 'Sent'), ('cancelled', 'Cancelled')], max_length=20)),
                ('archived_reason', models.TextField()),
                ('archived_date', models.DateTimeField()),
            ],
        ),
    ]

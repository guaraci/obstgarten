from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0009_photo'),
    ]

    operations = [
        migrations.AddField(
            model_name='orchard',
            name='is_public',
            field=models.BooleanField(default=True, verbose_name='This orchard is visible to the public', help_text="By checking this box, the orchard and its products become visible to the public. If it's unchecked, the orchard and its products are \"private\" and no longer publicly visible."),
        ),
        migrations.AddField(
            model_name='orchard',
            name='municipality',
            field=models.CharField(blank=True, max_length=100, verbose_name='Municipality'),
        ),
    ]

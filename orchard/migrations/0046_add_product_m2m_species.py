from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0045_booking_order_archive_models'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='fruit_species',
            field=models.ManyToManyField(blank=True, related_name='products', to='orchard.TreeSpecies', verbose_name='Fruits'),
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0047_migrate_prod_species'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='species',
        ),
    ]

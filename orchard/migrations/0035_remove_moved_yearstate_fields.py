from django.db import migrations, models
import orchard.models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0034_migrate_yearstate_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='yearstate',
            name='pay_method',
        ),
        migrations.RemoveField(
            model_name='yearstate',
            name='pick_end',
        ),
        migrations.RemoveField(
            model_name='yearstate',
            name='pick_start',
        ),
        migrations.RemoveField(
            model_name='yearstate',
            name='price',
        ),
    ]

from django.db import migrations


def migrate_fields(apps, schema_editor):
    Tree = apps.get_model('orchard', 'Tree')
    
    for tree in Tree.objects.all():
        latest_ys = tree.yearstate_set.order_by('-year').first()
        if latest_ys is not None:
            tree.availability = latest_ys.availability
            tree.treatment = latest_ys.treatment
            tree.save()


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0039_ys_fields_to_trees'),
    ]

    operations = [migrations.RunPython(migrate_fields, reverse_code=migrations.RunPython.noop)]

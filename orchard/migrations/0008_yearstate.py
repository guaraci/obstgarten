# Generated by Django 1.10.2 on 2016-10-26 15:25
from django.db import migrations, models
import django.db.models.deletion

from orchard.models import YearState


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0007_fruitcategory_color'),
    ]

    operations = [
        migrations.CreateModel(
            name='YearState',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.IntegerField(choices=YearState.YEAR_CHOICES, verbose_name='Year')),
                ('fruit_quant', models.CharField(blank=True, choices=YearState.QUANTITY_CHOICES, max_length=10, verbose_name='Fruit quantity')),
                ('treatment', models.TextField(blank=True, help_text='Treatments done or planned for this year', verbose_name='Tree treatments')),
                ('pick_start', models.CharField(blank=True, choices=[], max_length=5, verbose_name='Start of picking period')),
                ('pick_end', models.CharField(blank=True, choices=[], max_length=5, verbose_name='End of picking period')),
                ('tree', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='orchard.Tree', verbose_name='Tree')),
            ],
        ),
        migrations.RemoveField(
            model_name='tree',
            name='pick_dates',
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0030_tree_usages'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='typ',
            field=models.CharField(choices=[('self', 'Fruits to self pick'), ('picked', 'Picked fruits'), ('prod', 'Fruit products')], default='prod', max_length=10, verbose_name='Offer type'),
        ),
    ]

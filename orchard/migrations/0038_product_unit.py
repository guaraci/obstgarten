from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0037_avail_default_none'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='unit',
            field=models.CharField(blank=True, max_length=15, verbose_name='Unit'),
        ),
    ]

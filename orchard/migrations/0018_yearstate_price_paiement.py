from django.contrib.postgres.fields import ArrayField
from django.db import migrations, models
from orchard.models import ChoiceArrayField


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0017_add_booking'),
    ]

    operations = [
        migrations.AddField(
            model_name='orchard',
            name='real_owner_paiement_info',
            field=models.TextField(blank=True, help_text='CCP, Bank account, IBAN… to receive paiements if you charge for fruits', verbose_name='Paiement info'),
        ),
        migrations.AddField(
            model_name='yearstate',
            name='pay_method',
            field=ChoiceArrayField(base_field=models.CharField(choices=[('adv', 'Advance paiement'), ('cash', 'Cash on site')], max_length=5), null=True, size=None, verbose_name='Paiement method(s)'),
        ),
        migrations.AddField(
            model_name='yearstate',
            name='price',
            field=models.SmallIntegerField(blank=True, null=True, verbose_name='Price'),
        ),
    ]

from django.db import migrations


def migrate_fields(apps, schema_editor):
    Booking = apps.get_model('orchard', 'Booking')
    
    for book in Booking.objects.all():
        book.tree = book.yearstate.tree
        book.year = book.yearstate.year
        book.save()


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0041_booking_tree_year'),
    ]

    operations = [migrations.RunPython(migrate_fields, reverse_code=migrations.RunPython.noop)]

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orchard', '0010_orchard_public_municip'),
    ]

    operations = [
        migrations.AddField(
            model_name='orchard',
            name='real_owner_addr',
            field=models.TextField(blank=True, verbose_name='Address'),
        ),
        migrations.AddField(
            model_name='orchard',
            name='real_owner_email',
            field=models.EmailField(blank=True, max_length=254, verbose_name='email address'),
        ),
        migrations.AddField(
            model_name='orchard',
            name='real_owner_is_contact',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='orchard',
            name='real_owner_name',
            field=models.CharField(blank=True, max_length=100, verbose_name='Name'),
        ),
        migrations.AddField(
            model_name='orchard',
            name='real_owner_phone',
            field=models.CharField(blank=True, max_length=30, verbose_name='Phone'),
        ),
    ]

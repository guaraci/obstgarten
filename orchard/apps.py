from django.apps import AppConfig
from django.conf import settings
from django.templatetags.static import static


class OrchardConfig(AppConfig):
    name = 'orchard'

    def ready(self):
        try:
            settings.TINYMCE_DEFAULT_CONFIG.update(content_css=static('css/main.css'))
        except ValueError:
            # In some circunstances, static files are not ready and will produce
            # an error (typically before the first call to collectstatic).
            pass

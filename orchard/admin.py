from django.contrib import admin
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.forms import OSMWidget
from django.db.models import Count
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _
from parler.admin import TranslatableAdmin

from .models import (
    Booking, FruitCategory, Orchard, Photo, Product, ProductOrder, SavedSearch, Tree,
    TreeSpecies, YearState,
)


@admin.register(Orchard)
class OrchardAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_display = ('name', 'owner', 'municipality', 'is_public')
    list_filter = ('is_public',)
    # autocomplete_fields in 2.0 ?
    raw_id_fields = ['municipality']


@admin.register(SavedSearch)
class SavedSearchAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'user', 'interval', 'found_trees']

    def found_trees(self, obj):
        return len(obj.last_tree_list)


class YearStateInline(admin.TabularInline):
    model = YearState
    extra = 1


@admin.register(Tree)
class TreeAdmin(admin.ModelAdmin):
    search_fields = ['variety']
    formfield_overrides = {
        gis_models.PointField: {'widget': OSMWidget},
    }
    inlines = [YearStateInline]


@admin.register(FruitCategory)
class FruitCategoryAdmin(TranslatableAdmin):
    list_display = ('__str__', 'color')


@admin.register(TreeSpecies)
class TreeSpeciesAdmin(TranslatableAdmin):
    list_display = ('__str__', 'fruit_translated', 'color_sample')

    def fruit_translated(self, obj):
        return obj.safe_translation_getter('fruit', str(obj.pk))

    def color_sample(self, obj):
        return format_html(
            '<span style="background-color: {}; width: 35px; height: 20px; display: block;"></span>', obj.color
        )


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = ('user', 'tree', 'orchard', 'year', 'status')
    list_filter = ('status',)

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('user', 'tree', 'tree__orchard')

    def orchard(self, obj):
        return format_html(
            '<a href="{}">{}</a>', obj.tree.orchard.get_absolute_url(), obj.tree.orchard
        )
    orchard.short_description = _('Orchard')


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'orchard', 'typ', 'availability', 'num_orders')

    def get_queryset(self, request):
        return super().get_queryset(request).annotate(num_orders=Count('productorder'))

    def num_orders(self, obj):
        return obj.num_orders


@admin.register(ProductOrder)
class ProductOrderAdmin(admin.ModelAdmin):
    list_display = ('product', 'orchard', 'user', 'quantity', 'order_date', 'status')
    list_filter = ('status',)

    def orchard(self, obj):
        return format_html(
            '<a href="{}">{}</a>', obj.product.orchard.get_absolute_url(), obj.product.orchard
        )
    orchard.short_description = _('Orchard')


admin.site.register(Photo)

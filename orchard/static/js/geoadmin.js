// points are filled at template level
var otherPoints = [],
    selectedIcon = null,
    styleCache = {};

var otherPointsStyle = [new ol.style.Style({
  image: new ol.style.Circle({
      radius: 5,
      fill: new ol.style.Fill({
        color: '#aaa'
      }),
      stroke: new ol.style.Stroke({
        color: 'rgba(155, 0, 0, 0.3)',
        width: 2
      })
    })
})];

var clusteredCircle = new ol.style.Circle({
    radius: 14,
    fill: new ol.style.Fill({color: '#E61837'}),
    stroke: new ol.style.Stroke({
        color: 'rgba(255, 0, 0, 0.3)',
        width: 1
    })
});

var bdnmap = {
  init: function(div_id, startPoints, defaults) {
      this.plan_layer = ga.layer.create('ch.swisstopo.pixelkarte-grau');
      if (defaults.satellite) this.satellite_layer = ga.layer.create('ch.swisstopo.swissimage');
      var resolution = (defaults && defaults.resolution) ? defaults.resolution : 50;
      this.map = new ga.Map({
        target: div_id,
        layers: defaults.satellite ? [this.satellite_layer, this.plan_layer] : [this.plan_layer],
        view: new ol.View2D({
          resolution: resolution,
          center: [defaults.defaultLon || 620000, defaults.defaultLat || 215000]
        }),
        interactions: ol.interaction.defaults({ mouseWheelZoom:false }),
      });
      this.setFeatures(startPoints);

      if (otherPoints.length) {
          otherLayer = this.vectorLayerFromFeatures(
            this.readPoints(otherPoints), {'style': otherPointsStyle}
          );
          this.map.addLayer(otherLayer);
      }

      if (document.getElementById('map-popup')) {
          this.popup = new ol.Overlay({
              element: document.getElementById('map-popup'),
              //autoPan:true,  # buggy in OL current version
              positioning: "bottom-right"
          });
          this.map.addOverlay(this.popup);
      }
      var self = this;
      this.map.on('singleclick', evt => {
          self.displayFeatureInfo(evt, evt.pixel);
      });
      this.map.on('pointermove', evt => {
          if (!evt.dragging) {
              this.map.getTargetElement().style.cursor = this.map.hasFeatureAtPixel(this.map.getEventPixel(evt.originalEvent)) ? 'pointer' : '';
          }
      });
      if (defaults.detailsOnHover) {
        this.map.getViewport().addEventListener('mousemove', function(evt) {
            var pixel = self.map.getEventPixel(evt);
            var feat = self.map.forEachFeatureAtPixel(pixel, function(feature, layer) {
                return [feature, layer];
            });
            if (feat) {
                self.map.getTargetElement().style.cursor = 'pointer';
                self.displayFeatureInfo(evt, pixel);
            } else {
                self.map.getTargetElement().style.cursor = '';
                if (self.select.getFeatures().getLength() > 0) {
                    // Display back the currently selected feature
                    var geom = self.select.getFeatures().getArray()[0];
                    if (typeof load_object_details === "function")
                        load_object_details(geom.getProperties().features[0].get('pk'));
                } else {
                    if (typeof load_object_details === "function")
                        load_object_details(null);
                }
            }
        });
      }
      if (defaults.satellite) {
          document.getElementById("layeropacity").value = 0;
          this.update_opacity(0);
      }
  },

  readPoints: function(pts) {
      // Read a list of points and return a Feature Array.
      var JSONReader = new ol.format.GeoJSON(),
          WKTReader = new ol.format.WKT(),
          features = new Array();
      for (var i=0; i < pts.length; i++) {
          if (pts[i] != '') {
              var reader = (typeof pts[i] === 'object' || pts[i][0] == '{') ? JSONReader : WKTReader;
              features.push(reader.readFeature(pts[i]));
          }
      }
      return features;
  },

  vectorLayerFromFeatures: function(features, options) {
      var source = new ol.source.Vector({
          features: features
      });
      var clusterSource = null;
      if (features.length > 1) {
          clusterSource = new ol.source.Cluster({
              distance: 24,
              source: source
          });
          source = clusterSource;
      }
      return new ol.layer.Vector({
        source: source,
        style: options.style || this.getStyle
      });
  },

  getStyle: function(feature, resolution) {
    var multFeats = feature.get('features');
    var icon = null,
        color = null,
        image = null,
        text = null,
        defaultColor = 'rgba(255, 244, 0, 0.8)';
    if (multFeats) {
        var length = multFeats.length;
        if (length > 1) {
            // Clustered source
            text = new ol.style.Text({
                text: length.toString(),
                fill: new ol.style.Fill({
                  color: '#fff'
                }),
                scale: 2
            });
            image = clusteredCircle;
            var styleKey = 'clustered' + length.toString();
        } else {
            icon = multFeats[0].get('icon');
            color = multFeats[0].get('color') || defaultColor;
            var styleKey = 'main' + icon + color;
        }
    } else {
        icon = feature.get('icon');
        color = feature.get('color') || defaultColor;
        var styleKey = 'main' + icon + color;
        var length = 1;
    }
    var styleArray = styleCache[styleKey];
    if (!styleArray) {
        if (icon) {
            image = new ol.style.Icon({
                //scale: length < 2 ? 0.6 : (length < 4 ? 0.7 : (length < 7 ? 0.8 : 1.0)),
                scale: 0.7,
                src: icon
            });
        } else if (image === null) {
            image = new ol.style.Circle({
              radius: 8,
              fill: new ol.style.Fill({color: color}),
              stroke: new ol.style.Stroke({
                color: 'rgba(255, 0, 0, 0.3)',
                width: 2
              })
            });
        }
        styleArray = [new ol.style.Style({
          fill: new ol.style.Fill({
            color: 'rgba(255, 244, 0, 0.8)'
          }),
          stroke: new ol.style.Stroke({
            color: 'rgba(255, 0, 0, 0.3)',
            width: 2
          }),
          image: image,
          text: text
        })];
        styleCache[styleKey] = styleArray;
    }
    return styleArray
  },
  overlayStyle: function() {
    if (selectedIcon) {
        var img = new ol.style.Icon({
            src: selectedIcon,
            scale: 0.7
        });
    } else {
        var img = new ol.style.Circle({
            radius: 8,
            fill: new ol.style.Fill({
              color: [0, 153, 255, 1]
            }),
            stroke: new ol.style.Stroke({
              color: [255, 255, 255, 0.75],
              width: 1.5
            })
        });
    }
    return new ol.style.Style({
        image: img,
        zIndex: 100000
    });
  },

  setFeatures: function(points, center=null) {
    // Set main features for the map
    // delete features from point layer if any
    if (this.pointsLayer) this.map.removeLayer(this.pointsLayer);
    // set new layer/features from points
    this.features = this.readPoints(points);
    this.pointsLayer = this.vectorLayerFromFeatures(this.features, {});

    if (this.pointsLayer.getSource() instanceof ol.source.Cluster) {
        var extent = this.pointsLayer.getSource().getSource().getExtent();
    } else {
        var extent = this.pointsLayer.getSource().getExtent();
    }
    this.map.addLayer(this.pointsLayer);
    if (this.features.length > 1) {
        this.map.getView().fit(extent, this.map.getSize());
    } else if (this.features.length > 0) {
        this.map.getView().setCenter(this.features[0].getGeometry().getCoordinates());
    } else {
        this.map.getView().setCenter(center || [620000, 215000]);
        this.map.getView().setZoom(2);
    }
    this.select = new ol.interaction.Select({
        style: this.overlayStyle(),
        layers: [this.pointsLayer]
    });
    this.map.addInteraction(this.select);
  },

  make_editable: function() {
    var self = this;
    var draw = new ol.interaction.Draw({
        source: this.pointsLayer.getSource(),
        type: 'Point'
    });
    var modify = new ol.interaction.Modify({
        features: this.select.getFeatures(),
        style: self.overlayStyle()
    });
    var addModify = function(features) {
        features[0].on('change', function(ev) {
            var coords = ev.target.getGeometry().getCoordinates();
            set_form_coordinates(coords[0], coords[1]);
        });
        self.map.addInteraction(modify);
    };
    if (this.features.length) {
        addModify(self.features);
    } else {
        // Allow to add new point, and only one point
        self.map.addInteraction(draw);
        draw.on('drawend', function(ev) {
          var coords = ev.feature.getGeometry().getCoordinates();
          set_form_coordinates(coords[0], coords[1]);
          self.map.removeInteraction(draw);
          addModify([ev.feature]);
        });
    }
  },

  findFeatures: function(pixel) {
      var features = [];
      this.map.forEachFeatureAtPixel(pixel, function(feature, layer) {
        features.push(feature);
      });
      return features;
  },

  displayFeatureInfo: function(evt, pixel) {
      var coordinate = evt.coordinate,
          features = this.findFeatures(pixel);

      if (features.length === 0) {
          return;
      }

      var feature = features[0];
      if (this.popup) var element = this.popup.getElement();
      if (feature) {
          var allFeats = [feature];
          if (this.pointsLayer.getSource() instanceof ol.source.Cluster) {
              allFeats = feature.get('features');
          }
          if (allFeats.length == 1) {
              if (typeof load_object_details === "function")
                load_object_details(allFeats[0].get('pk'));
                var content = allFeats[0].get('html');
          } else {
              // Zoom until features are separated
              var extent = new ol.extent.createEmpty();
              allFeats.forEach(function(f, index, array){
                  ol.extent.extend(extent, f.getGeometry().getExtent());
              });
              this.map.getView().fit(extent, this.map.getSize());
              this.select.getFeatures().clear();
              evt.stopPropagation();
              return;
          }

            if (this.popup) {
                element.style.display = 'none';
                this.popup.setPosition(coordinate);

                if (content !== undefined && content.length > 0) {
                    element.innerHTML = content;
                }

                if (allFeats.length == 1) {
                    element.style.display = 'block';
                }
            }
      } else {
         if (typeof load_object_details === "function")
            load_object_details(null);
         if (this.popup) element.style.display = 'none';
      }
  },

  set_map_coordinates: function(x, y) {
    if (x == null) x = parseFloat(document.getElementById("id_longitude").value);
    if (y == null) y = parseFloat(document.getElementById("id_latitude").value);
    if (x == "" || y == "") return;
    var new_point = new ol.geom.Point([x, y]);
    var features = this.pointsLayer.getSource().getFeatures();
    if (features.length) {
        features[0].setGeometry(new_point);
    } else {
        this.pointsLayer.getSource().addFeature([ol.Feature(new_point)]);
    }
    this.map.getView().setCenter(new_point.getCoordinates());
  },

  set_coord_from_browser: function() {
    if (!navigator.geolocation) return;
    var saved_content = $("div#current_loc").html();
    var self = this;
    $("div#current_loc").html(gettext("Trying to obtain your geolocation data..."));
    navigator.geolocation.getCurrentPosition(function(position) {
        var x = position.coords.longitude;
        var y = position.coords.latitude;
        set_form_coordinates(x, y);
        self.set_map_coordinates(x, y);
        $("div#current_loc").html(saved_content);
    }, function(error) {
        alert(typeof error.message == 'string' ? error.message : gettext("Sorry, an error occurred"));
        $("div#current_loc").html(saved_content);
    });
  },

  update_opacity: function(value) {
      this.plan_layer.setOpacity(value);
  }
}


function set_form_coordinates(x, y) {
    longEl = document.getElementById("id_longitude");
    if (longEl) longEl.value = x;
    latEl = document.getElementById("id_latitude");
    if (latEl) latEl.value = y;
    pointArea = document.getElementById("id_point");
    if (pointArea) {
        wktWriter = new ol.format.WKT();
        pointArea.value = "SRID=21781;" + wktWriter.writeFeature(new ol.Feature({
            geometry: new ol.geom.Point([x, y])
        }));
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    opacity_el = document.getElementById("layeropacity");
    if (opacity_el) {
        opacity_el.addEventListener("input", function (ev) {
            bdnmap.update_opacity(this.value);
        });
    }
});

document.addEventListener("DOMContentLoaded", function(event) {
    var saveLink = document.getElementById('savesearch-link');
    if (saveLink) {
        saveLink.addEventListener("click", function (ev) {
            ev.preventDefault();
            document.getElementById('savesearch-form').style.display = 'block';
        });
    }

    bdnmap.init('map', startPoints, {resolution: 6, format: 'JSON', detailsOnHover: false});

    const fromLoc = document.querySelector('#id_from_loc');
    $(fromLoc).autocomplete({source: fromLoc.dataset.url, minLength: 2});

    document.querySelector('#search-form').addEventListener('change', function (event) {
        search_form();
    });
});

function search_form(fromPpostate=false) {
    var qs = $('#search-form').serialize();
    if (!fromPpostate)  history.pushState({query: qs}, 'search', '?' + qs)
    $.get($('#search-form').attr('action') + '?' + qs, function(data) {
        const fromLoc = document.querySelector('#id_from_loc');
        fromLoc.classList.remove('error');
        if (data.errors) {
            if (data.errors.from_loc) fromLoc.classList.add('error');
            else if (data.errors.__all__) {
                bdnmap.setFeatures(startPoints);
                $('div#result-count').html('');
                $('#orchard-list').html('');
            }
            return;
        }
        // Here set results on map
        points = [];
        for (var i=0; i < data.orchards.length; i++) {
            points.push(data.orchards[i].orchard.geojson);
            $("#header ul").append('<li></li>');
        }
        bdnmap.setFeatures(points, data.center)
        $('div#result-count').html(data.total_str);

        if(data.orchard_list.length > 1) {
            $('#result-list').show();
            $('#orchard-list').html(data.orchard_list);
        } else {
            $('#result-list').hide();
        }
        
        $('div#savesearch-div').show();
        $('form#savesearch-form input#id_name').val(data.save_name);
        $('form#savesearch-form input#id_form_data').val(qs);
    });
}

window.onpopstate = function(ev) {
    var form_vals = ev.state.query.split('&');
    for (i=0; i<form_vals.length; i++) {
        var keyval = form_vals[i].split("=");
        document.getElementById('id_' + keyval[0]).value = keyval[1];
    }
    search_form(fromPpostate=true);
};

function load_object_details(pk) {
    var element = bdnmap.popup.getElement();
    var url = orchardDetailsURL.replace('\/0\/', '\/' + pk + '\/');
    $(element).load(url, function () {
        $('img.closebox').on('click', function (ev) {
            $(element).hide();
        });
    });
}

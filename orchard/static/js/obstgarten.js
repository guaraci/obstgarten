function sortUnorderedList(ul, sortDescending) {
  if(typeof ul == "string")
    ul = document.getElementById(ul);

  // Idiot-proof, remove if you want
  if(!ul) {
    alert("The UL object is null!");
    return;
  }

  // Get the list items and setup an array for sorting
  var lis = ul.getElementsByTagName("LI");
  var vals = [];

  // Populate the array
  for(var i = 0, l = lis.length; i < l; i++)
    vals.push([lis[i].textContent, lis[i].innerHTML]);

  // Sort it
  vals.sort();

  // Sometimes you gotta DESC
  if(sortDescending)
    vals.reverse();

  // Change the list on the page
  for(var i = 0, l = lis.length; i < l; i++)
    lis[i].innerHTML = vals[i][1];
}

function sortSelect(selElem) {
    var tmpAry = new Array();
    for (var i=0; i<selElem.options.length; i++) {
        tmpAry[i] = new Array();
        tmpAry[i][0] = selElem.options[i].text;
        tmpAry[i][1] = selElem.options[i].value;
        tmpAry[i][2] = selElem.options[i].selected;
    }
    tmpAry.sort();
    while (selElem.options.length > 0) {
        selElem.options[0] = null;
    }
    var selIndex = 0;
    for (var i=0;i<tmpAry.length;i++) {
        var op = new Option(tmpAry[i][0], tmpAry[i][1]);
        if (tmpAry[i][2]) selIndex = i;
        selElem.options[i] = op;
    }
    selElem.selectedIndex = selIndex;
    return;
}

var getClosest = function (elem, selector) {
    if (!Element.prototype.matches) {
        // IE polyfill
        Element.prototype.matches = Element.prototype.msMatchesSelector;
    }
    // Get closest match
    for ( ; elem && elem !== document; elem = elem.parentNode ) {
        if (elem.matches(selector)) return elem;
    }
    return null;
};

document.addEventListener("DOMContentLoaded", function(event) {
    var dropMenu = document.querySelector("ul.dropdown > li");
    if (dropMenu) {
        dropMenu.addEventListener('click', function (ev) {
            if (ev.target == this) {
                ev.preventDefault();
                this.classList.toggle('shown');
                ev.stopPropagation();
            }
        });
    }
    document.addEventListener('click', function (ev) {
        // Close open menu when clicking anywhere else in the page.
        var openMenu = document.querySelector("li.shown");
        if (openMenu) openMenu.classList.toggle('shown');
        // If any clicked button has a data-confirm property, ask for confirmation.
        if (ev.target.tagName == "BUTTON" && ev.target.dataset.confirm !== undefined) {
            if (!confirm(ev.target.dataset.confirm)) ev.preventDefault();
        }
    });
    document.addEventListener('submit', function (ev) {
        // Case where the data-confirm is set on the form itself
        if (ev.target.dataset.confirm !== undefined) {
            if (!confirm(ev.target.dataset.confirm)) ev.preventDefault();
        }
    });
    var cancelBtn = document.getElementById('cancelBack');
    if (cancelBtn) {
        cancelBack.addEventListener('click', function (ev) { window.history.back(); });
    }

    var readMore = document.querySelectorAll('a.readmore');
    for (var i=0; i<readMore.length; i++) {
      readMore[i].addEventListener('click', function (ev) {
          ev.preventDefault();
          var shortDiv = getClosest(this, '.description-short')
          shortDiv.nextSibling.nextSibling.style.display = 'block';
          shortDiv.style.display = 'none';
      });
    }
    var readLess = document.querySelectorAll('a.readless');
    for (var i=0; i<readLess.length; i++) {
      readLess[i].addEventListener('click', function (ev) {
          ev.preventDefault();
          var longDiv = getClosest(this, '.description-long')
          longDiv.previousSibling.previousSibling.style.display = 'block';
          longDiv.style.display = 'none';
      });
    }

});

document.addEventListener('DOMContentLoaded', function(event) {
    new Canvi({
        content: '.js-canvi-content',
        navbar: '.js-canvi-navbar--right',
        openButton: '.js-canvi-open-button--right',
        position: 'right',
        pushContent: false,
        isDebug: false,
        speed: '0.2s',
        width: '70vw',
        responsiveWidths: [
            {
                breakpoint: '600px',
                width: '340px'
            }
        ]
    });

    setTimeout(function() {
        accordionHelper();
    }, 0);

    window.addEventListener('resize', function(event){
        accordionHelper();
    });

    var accordions = document.querySelectorAll( '.accordion-item__title' );

    for (var i = 0; i < accordions.length; i++) {
        accordions[i].addEventListener('click', function() {
            var _self = this.dataset.id,
                style = document.createElement('style'),
                height = document.querySelector('.accordion-item[data-id="' + this.dataset.id + '"]').querySelector('.accordion-item__content-helper').offsetHeight;

            document.querySelector('.accordion-item[data-id="' + this.dataset.id + '"] .accordion-item__content').removeAttribute('style');

            document.getElementsByTagName('head')[0].removeChild(document.querySelector('style[id="' + this.dataset.id + '"]'));

            style.setAttribute('id', this.dataset.id);
            style.type = 'text/css';
            style.innerHTML = '.accordion-item[data-id="' + this.dataset.id + '"].is-open .accordion-item__content { max-height: ' +  height + 'px; } .accordion-item[data-id="' + this.dataset.id + '"] .accordion-item__content { transition-duration: ' + height / 2000 + 's; }';
            document.getElementsByTagName('head')[0].appendChild(style);

            setTimeout(function() {
                document.querySelector('.accordion-item[data-id="' + _self + '"]').classList.toggle('is-open');
            }, 0);
        });
    }
});

function accordionHelper() {
    var accordions = document.querySelectorAll( '.accordion-item__title' );

    for (var i = 0; i < accordions.length; i++) {
        var height = document.querySelector('.accordion-item[data-id="' + accordions[i].dataset.id + '"]').querySelector('.accordion-item__content-helper').offsetHeight,
            style = document.createElement('style');

        style.setAttribute('id', accordions[i].dataset.id);
        style.type = 'text/css';
        style.innerHTML = '.accordion-item[data-id="' + accordions[i].dataset.id + '"].is-open .accordion-item__content { max-height: ' +  height + 'px; } .accordion-item[data-id="' + accordions[i].dataset.id + '"] .accordion-item__content { transition-duration: ' + height / 2000 + 's; }';
        document.getElementsByTagName('head')[0].appendChild(style);

    }
}

document.addEventListener('DOMContentLoaded', function () {
    var closer = document.querySelector('.site-hero__close');

    if (closer) {
        closer.addEventListener('click', function (event) {
            document.cookie = 'about_closed=true;path=/';
            document.querySelector('.site-hero').style.display = 'none';
        });
    }
});

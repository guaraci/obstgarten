from collections import Counter, defaultdict
from datetime import date, datetime

from django.conf import settings
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.gis.db.models.aggregates import Collect
from django.contrib.gis.db.models.functions import Centroid
from django.core.exceptions import PermissionDenied
from django.db.models import Case, Count, IntegerField, When
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.template import loader
from django.templatetags.static import static
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.html import strip_tags
from django.utils.translation import override, gettext_lazy as _, ngettext
from django.views.generic import (
    CreateView, DeleteView, DetailView, FormView, ListView, TemplateView,
    UpdateView, View,
)

from .forms import (
    OrchardForm, OrchardContactForm, ProductForm, ProductOrderForm,
    ProductOrderConfirmForm, SearchForm, SearchSaveForm, TreeCopyModifsForm,
    TreeForm, PhotosFormSet, YearStateFormSet,
)
from .mail import send_mail
from .models import Booking, Orchard, Product, ProductOrder, SavedSearch, Tree

MAP_SRID = 21781


supporters = [
    {'title': "BLW - OFAG",
     'url': 'https://www.blw.admin.ch/blw/de/home/nachhaltige-produktion/pflanzliche-produktion/pflanzengenetische-ressourcen/nap-pgrel.html',
     'img_src': static('logos/logo-ofag.png'),
    },
    {'title': "Stiftung Mercator Schweiz",
     'url': 'https://www.stiftung-mercator.ch/',
     'img_src': static('logos/logo-mercator.jpg'),
    },
    {'title': "Guaraci Forest Consulting",
     'url': 'http://guaraci.ch/',
     'img_src': static('logos/logo-guaraci.gif'),
    },
    {'title': "We Make It",
     'url': 'https://wemakeit.com/projects/meinobstgarten-ch',
     'img_src': static('logos/logo-wemakeit.png'),
    }
]


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        now = datetime.now().strftime("%m.%d")
        context.update({
            'supporters': supporters,
            'pickables': Tree.objects.filter(orchard__is_public=True, pick_start__lte=now, pick_end__gte=now).order_by('-species').all(),
            'about_closed': True if 'about_closed' in self.request.COOKIES else False,
        })
        return context


class TitleViewMixin:
    """View with title attribute added to context."""
    title = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'title': self.title,
        })
        return context


class SimpleTemplateView(TitleViewMixin, TemplateView):
    pass


class AboutView(SimpleTemplateView):
    template_name = 'about.html'
    title = _("About meinobstgarten.ch")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'supporters': supporters,
        })
        return context


class MapView(SimpleTemplateView):
    template_name = 'map.html'
    title = _("Orchard map")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'orchards': Orchard.objects.visible(self.request.user
                        ).annotate(ext=Collect('trees__point')
                        ).annotate(point=Centroid('ext')).exclude(point__isnull=True),
        })
        return context


class OfferView(SimpleTemplateView):
    template_name = 'offering.html'
    title = _("Offering fruits")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            # In Django 2.0:  num_self=Count('product', filter=Q(typ='self'))
            context['orchards'] = self.request.user.orchard_set.all(
                ).annotate(num_trees=Count('trees'),
                           num_self=Count(Case(When(products__typ='self', then=1),
                                output_field=IntegerField())),
                           num_picked=Count(Case(When(products__typ='picked', then=1),
                                output_field=IntegerField())),
                           num_prod=Count(Case(When(products__typ='prod', then=1),
                                output_field=IntegerField())),
                ).order_by('name')
        return context


class CollectView(SimpleTemplateView):
    template_name = 'collect/collecting.html'
    title = _("Collecting fruits")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        trees = None
        products = None
        orchards = None
        if self.request.GET:
            search_form = SearchForm(data=self.request.GET)
            if search_form.is_valid():
                trees, products = search_form.search()
            if trees is not None:
                orchards = defaultdict(
                    lambda: Counter(dict({tp: 0 for tp, _ in Product.PRODTYPE_CHOICES}, trees=0))
                )
                for tree in trees:
                    orchards[tree.orchard]['trees'] += 1
                for prod in products:
                    orchards[prod.orchard][prod.typ] += 1
                orchards = [(orch, cnt) for orch, cnt in orchards.items()]
        else:
            # Initially all visible orchards are shown on the page
            orchards = [(orch, None) for orch in Orchard.objects.visible(self.request.user
                ).annotate(ext=Collect('trees__point')
                ).annotate(point=Centroid('ext'))
            ]
            search_form = SearchForm()

        context.update({
            'search_form': search_form,
            'orchards': orchards,
        })
        try:
            center = search_form.cleaned_data.get('from_loc').center
        except AttributeError:
            pass
        else:
            center.transform(MAP_SRID)
            context['center'] = center.coords

        if self.request.user.is_authenticated:
            context['save_form'] = SearchSaveForm()
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest':  # prev. is_ajax
            if not context['search_form'].is_valid():
                return JsonResponse({'errors': context['search_form'].errors})
            return JsonResponse({
                'orchards': [{
                    'orchard': {
                    'get_absolute_url': orch.get_absolute_url(),
                    'name': str(orch),
                    'geojson': orch.geojson(target_srid=MAP_SRID),
                }, 'counter': cnt} for orch, cnt in context['orchards']],
                'total_str': ngettext("Your search returned %(num)s result.",
                                      "Your search returned %(num)s results.",
                                      len(context['orchards'])) % {'num': len(context['orchards'])},
                'orchard_list': loader.render_to_string(
                    'orchard_search_list.html', {'orchards': context['orchards']}
                ),
                'center': context.get('center'),
                'save_name': context['search_form'].name_from_data()
            })
        else:
            return super().render_to_response(context, **response_kwargs)


class SearchSaveView(LoginRequiredMixin, CreateView):
    model = SavedSearch
    form_class = SearchSaveForm

    def get_success_url(self):
        return reverse('savedsearch-list', args=[self.request.user.pk])

    def form_valid(self, form):
        self.object = form.save(self.request.user)
        messages.success(self.request, _("Your search was successfully saved."))
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        messages.error(self.request, _("Sorry, something went wrong with your request"))
        return HttpResponseRedirect(reverse('collect'))


class SearchSaveDeleteView(LoginRequiredMixin, DeleteView):
    model = SavedSearch

    def get_success_url(self):
        messages.success(self.request, _("The saved search was successfully deleted."))
        return reverse('savedsearch-list', args=[self.request.user.pk])

    def get_object(self):
        return get_object_or_404(self.model, pk=self.request.POST['searchid'])


class OrchardNewView(LoginRequiredMixin, CreateView):
    model = Orchard
    mode = 'add'
    form_class = OrchardForm
    template_name = 'orchard_edit.html'

    def get_form_kwargs(self, **kwargs):
        kwargs = super().get_form_kwargs(**kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        response = super().form_valid(form)
        # Warn the site admin about the new orchard
        hostname = _("meinobstgarten.ch")
        send_mail(
            '[%s] New orchard created' % hostname,
            'The following new orchard was created by %(user)s:\n'
            'https://www.%(host)s%(url)s' % {
                'user': self.object.owner,
                'host': hostname,
                'url': self.object.get_absolute_url(),
            },
            settings.SITE_EMAIL, [settings.SITE_EMAIL]
        )
        return response

    def get_success_url(self):
        return reverse('orchard', args=[self.object.pk])


class OrchardView(DetailView):
    model = Orchard
    template_name = 'orchard.html'

    def dispatch(self, *args, **kwargs):
        orchard = self.get_object()
        if not orchard.can_view(self.request.user):
            raise PermissionDenied
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        editable = self.object.can_edit(self.request.user)
        products = self.object.products.all().order_by('-typ', '-availability') if editable else self.object.products.filter(availability=True).order_by('-typ')
        trees = self.object.trees.all().select_related('species__category'
            ).prefetch_related('yearstate_set', 'booking_set'
            ).order_by('species__category', 'species', 'variety')
        this_year = date.today().year
        context.update({
            'breadcrumb': [(_('Home'), reverse('home')), (_("Offering fruits"), reverse('offer'))],
            'title': self.object.name,
            'trees': trees,
            'trees_bookable': trees.filter(availability='whole'),
            'has_available_trees': any(tree.booking_available(this_year) for tree in trees),
            'products': products,
            'editable': editable,
            'contact_form': OrchardContactForm(),
        })
        return context


class OrchardContactSend(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        orchard = get_object_or_404(Orchard, pk=kwargs['pk'])
        form = OrchardContactForm(data=request.POST)
        if form.is_valid():
            with override(orchard.owner.language):
                hostname = _("meinobstgarten.ch")
                body = loader.render_to_string('email/orchard_contact.txt', {
                    'hostname': hostname,
                    'name': str(request.user),
                    'email': request.user.email,
                    'manager_name': settings.SITE_MANAGER,
                    'manager_email': settings.SITE_EMAIL,
                    'message': request.POST.get('message'),
                }, using='django-text')
                send_mail(
                    _('[%s] Message from a user') % hostname,
                    body,
                    settings.SITE_EMAIL,
                    [orchard.get_contact()['email']],
                    reply_to=[request.user.email],
                )
            messages.success(
                request,
                _("Your message was successfully sent to the orchard owner.")
            )
        else:
            messages.error(request, _("Sorry your message is not valid."))
        return HttpResponseRedirect(orchard.get_absolute_url())


class OrchardMapDetails(DetailView):
    model = Orchard
    template_name = 'orchard_map_details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        photo = self.object.photos.first()
        context.update({
            'photo': photo.image if photo is not None else '',
        })
        return context


class OrchardOverview(DetailView):
    model = Orchard
    template_name = 'orchard_overview.html'
    context_object_name = 'orchard'


class OrchardEditView(UpdateView):
    model = Orchard
    mode = 'edit'
    form_class = OrchardForm
    template_name = 'orchard_edit.html'

    def get_object(self):
        orchard = super().get_object()
        if not orchard.can_edit(self.request.user):
            raise PermissionDenied
        return orchard

    def get_form(self):
        form = super().get_form()
        if self.request.method in ('POST', 'PUT'):
            self.photo_fset = PhotosFormSet(self.request.POST, self.request.FILES, instance=form.instance)
        else:
            self.photo_fset = PhotosFormSet(instance=form.instance)
        return form

    def form_valid(self, form):
        if self.photo_fset.is_valid():
            self.object = form.save()
            self.photo_fset.instance = self.object
            self.photo_fset.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [
                (_('Home'), reverse('home')),
                (self.object.name, reverse('orchard', args=[self.object.pk])),
            ],
            'title': _("Editing an orchard"),
            'photosformset': self.photo_fset,
        })
        return context


class OrchardDeleteView(DeleteView):
    model = Orchard
    template_name = 'orchard_confirm_delete.html'
    success_url = reverse_lazy('offer')

    def get_object(self):
        orchard = super().get_object()
        if not orchard.can_edit(self.request.user):
            raise PermissionDenied
        self.success_message = _('The orchard "%s" has been deleted.') % orchard
        return orchard

    def has_pending_stuff(self):
        return (
            ProductOrder.objects.filter(product__orchard=self.orchard, status='ordered').count() > 0 or
            Booking.objects.filter(tree__orchard=self.orchard, status='planned').count() > 0
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.orchard = self.get_object()
        context['has_pending'] = self.has_pending_stuff()
        return context

    def form_valid(self, form):
        self.orchard = self.get_object()
        if self.has_pending_stuff():
            messages.error(
                self.request,
                'You cannot delete the orchard while it has pending product orders and bookings. '
                'Cancel them before you try again.'
            )
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', "/"))

        # Archive product orders
        for order in ProductOrder.objects.filter(product__orchard=self.orchard):
            order.archive(reason='Orchard was deleted.')
        # Archive bookings
        for booking in Booking.objects.filter(tree__orchard=self.orchard):
            booking.archive(reason='Orchard was deleted.')
        self.orchard.trees.all().delete()
        response = super().form_valid(form)
        messages.success(self.request, self.success_message)
        return response


class TreeDetailsView(DetailView):
    model = Tree
    template_name = 'tree_details.html'
    pk_url_kwarg = 'pk_tree'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [
                (_('Home'), reverse('home')),
                (self.object.orchard.name, reverse('orchard', args=[self.object.orchard.pk])),
            ],
            'title': self.object.species,
            'ystate': self.object.state_for_year(),
            'other_trees': self.object.orchard.trees.exclude(pk=self.object.pk),
        })
        return context


class TreeEditBaseMixin:
    model = Tree
    form_class = TreeForm
    template_name = 'tree_edit.html'
    success_message = ''

    def dispatch(self, *args, **kwargs):
        self.orchard = get_object_or_404(Orchard, pk=kwargs['pk'])
        if not self.orchard.can_edit(self.request.user):
            raise PermissionDenied
        return super().dispatch(*args, **kwargs)

    def get_form(self):
        form = super().get_form()
        year_fset_initial = [{
            'year': max(
                [date.today().year] +
                ([s.year + 1 for s in form.instance.yearstate_set.all()] if form.instance.pk else [])
            ),
        }]
        if self.request.method in ('POST', 'PUT'):
            self.year_fset = YearStateFormSet(
                self.request.POST, instance=form.instance, initial=year_fset_initial)
            self.photo_fset = PhotosFormSet(self.request.POST, self.request.FILES, instance=form.instance)
        else:
            self.year_fset = YearStateFormSet(instance=form.instance, initial=year_fset_initial)
            self.photo_fset = PhotosFormSet(instance=form.instance)
        return form

    def form_valid(self, form):
        if all([self.year_fset.is_valid(), self.photo_fset.is_valid()]):
            self.object = tree = form.save()
            self.year_fset.instance = tree
            self.year_fset.save()
            self.photo_fset.instance = tree
            self.photo_fset.save()
            # Similar to SuccessMessageMixin (note: we bypass super().form_valid)
            messages.success(self.request, self.success_message % {'tree': form.instance})
            msg = getattr(tree, '_message', None)
            if msg:
                messages.warning(self.request, msg)
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        if 'submit_and_continue' in self.request.POST:
            return reverse('tree_edit', args=[self.object.orchard.pk, self.object.pk])
        else:
            return reverse('orchard', args=[self.object.orchard.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [
                (_('Home'), reverse('home')),
                (self.orchard.name, reverse('orchard', args=[self.orchard.pk])),
            ],
            'title': _("Editing a tree"),
            'yearstateformset': self.year_fset,
            'photosformset': self.photo_fset,
        })
        if self.mode == 'add':
            context['other_trees'] = self.orchard.trees.all()
        else:
            context['other_trees'] = self.orchard.trees.exclude(pk=self.object.pk)
        return context


class TreeNewView(TreeEditBaseMixin, CreateView):
    mode = 'add'
    success_message = _("%(tree)s was created successfully")

    def get_initial(self):
        initial = super().get_initial()
        initial['orchard'] = self.orchard
        return initial


class TreeEditView(TreeEditBaseMixin, UpdateView):
    mode = 'edit'
    pk_url_kwarg = 'pk_tree'
    success_message = _("%(tree)s was changed successfully")

    def form_valid(self, form):
        response = super().form_valid(form)
        has_trees_from_same_variety = self.orchard.trees.filter(
            species=form.instance.species, variety=form.instance.variety).count() > 1
        changed_fields = ','.join(f for f in form.changed_data if f in form.allowed_copy_fields)
        if response.status_code > 299 and changed_fields and has_trees_from_same_variety:
            # redirect to TreeCopyModifsView
            response = response.__class__(
                reverse('tree_copymodifs', args=[self.orchard.pk, form.instance.pk]) + '?fields=%s&next=%s' % (changed_fields, response.url)
            )
        return response


class TreeCopyModifsView(LoginRequiredMixin, FormView):
    template_name = 'tree_copymodifs.html'
    form_class = TreeCopyModifsForm

    def dispatch(self, request, *args, **kwargs):
        self.tree = get_object_or_404(Tree, pk=kwargs['pk_tree'])
        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        initial['next'] = self.request.GET.get('next')
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['fields'] = self.request.GET.get('fields').split(',')
        kwargs['tree'] = self.tree
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['num'] = self.tree.orchard.trees.filter(
            species=self.tree.species, variety=self.tree.variety
        ).count() - 1
        return context

    def form_valid(self, form):
        if self.request.POST.get('no') == 'no':
            return HttpResponseRedirect(form.cleaned_data['next'])
        elif self.request.POST.get('yes') == 'yes':
            # copy field values from original tree to similar trees (same variety)
            other_trees = self.tree.orchard.trees.filter(
                species=self.tree.species, variety=self.tree.variety
            ).exclude(pk=self.tree.pk)
            field_names = [f for f in form.cleaned_data if form.cleaned_data[f] is True]
            for tree in other_trees:
                for field_name in field_names:
                    setattr(tree, field_name, getattr(self.tree, field_name))
                tree.save()
            messages.success(
                self.request,
                ngettext("Changes have been successfully applied to %d other tree",
                         "Changes have been successfully applied to %d other trees",
                         len(other_trees)
                ) % len(other_trees)
            )
            return HttpResponseRedirect(form.cleaned_data['next'])
        else:
            messages.error(self.request, _("Please, click either on Yes or No"))
            return self.form_invalid(form)


class TreeDuplicateView(TreeNewView):
    def get(self, request, *args, **kwargs):
        orig_tree = get_object_or_404(Tree, pk=self.kwargs['pk_tree'])
        orig_tree.pk = None
        orig_tree.point = None
        self.object = orig_tree
        self.orchard = orig_tree.orchard
        return self.render_to_response(self.get_context_data())


class TreeDeleteView(DeleteView):
    pk_url_kwarg = 'pk_tree'
    model = Tree
    success_message = _("%(tree)s was deleted successfully")

    def dispatch(self, *args, **kwargs):
        self.orchard = get_object_or_404(Orchard, pk=kwargs['pk'])
        if not self.orchard.can_edit(self.request.user):
            raise PermissionDenied
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse('orchard', args=[self.kwargs['pk']])


class TreeMapDetails(DetailView):
    model = Tree
    template_name = 'tree_map_details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        photo = self.object.photos.first()
        context.update({
            'photo': photo.image if photo is not None else '',
        })
        return context


class TreeBookingView(LoginRequiredMixin, View):
    template_name = 'tree_booking_form.html'

    def dispatch(self, request, *args, **kwargs):
        self.tree = get_object_or_404(Tree, pk=kwargs['pk'])
        self.year = int(kwargs['year'])
        if not self.tree.booking_available(self.year):
            messages.error(request, _("Sorry, someone booked that tree before you!"))
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', "/"))
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, context={
            'tree': self.tree,
            'year': self.year,
        })

    def post(self, request, *args, **kwargs):
        # FIXME: not totally concurrency-safe. unable to set unicity at DB level because of
        # the dependance on status.
        Booking.objects.create(tree=self.tree, year=self.year, user=request.user, status='planned')
        messages.success(request,
            _("You successfully booked fruit collecting for tree %(tree)s in orchard %(orchard)s") % {
            'tree': self.tree,
            'orchard': self.tree.orchard
        })
        orchard_contact = self.tree.orchard.get_contact()
        # Send mail to both collector and orchard 'owner'
        with override(request.user.language):
            hostname = _("meinobstgarten.ch")
            body = loader.render_to_string('email/tree_booked_client.txt', {
                'hostname': hostname,
                'tree': self.tree,
                'pick_date_string': self.tree.pick_date_string(self.year),
                'pick_details': strip_tags(self.tree.orchard.pick_details),
                'orchard': self.tree.orchard,
                'contact': orchard_contact,
            }, using='django-text')
            send_mail(
                _('[%s] Fruit collection confirmation') % hostname,
                body,
                settings.SITE_EMAIL,
                [request.user.email],
            )
        with override(self.tree.orchard.owner.language):
            hostname = _("meinobstgarten.ch")
            body = loader.render_to_string('email/tree_booked_owner.txt', {
                'hostname': hostname,
                'tree': self.tree,
                'orchard': self.tree.orchard,
                'user': request.user,
            }, using='django-text')
            send_mail(
                _('[%s] Fruit collection booked') % hostname,
                body,
                settings.SITE_EMAIL,
                [orchard_contact['email']],
            )
        return HttpResponseRedirect(reverse('bookings-list', args=[request.user.pk]))


class BookingDeleteView(LoginRequiredMixin, DeleteView):
    """Booking deletion from the user side."""
    model = Booking

    def get_object(self):
        return get_object_or_404(self.model, pk=self.request.POST['bookingid'])

    def get_success_url(self):
        return reverse('bookings-list', args=[self.request.user.pk])

    def form_valid(self, form):
        if self.request.user != self.object.user:
            raise PermissionDenied("Deletion by the booking user only")
        if not self.object.can_delete():
            messages.error(self.request, _("Passed booking cannot be deleted."))
            return HttpResponseRedirect(self.get_success_url())

        orchard = self.object.tree.orchard
        hostname = _("meinobstgarten.ch")
        orchard_contact = orchard.get_contact()
        body = loader.render_to_string('email/tree_booked_cancel.txt', {
            'hostname': hostname,
            'tree': self.object.tree,
            'orchard': self.object.tree.orchard,
            'user': self.object.user,
        }, using='django-text')

        self.object.delete()

        send_mail(
            _('[%s] Fruit collection cancelled') % hostname,
            body,
            settings.SITE_EMAIL,
            [orchard_contact['email']],
        )
        return HttpResponseRedirect(self.get_success_url())


class ProductEditBaseMixin(SuccessMessageMixin):
    model = Product
    form_class = ProductForm
    template_name = 'product_edit.html'

    def dispatch(self, *args, **kwargs):
        self.orchard = get_object_or_404(Orchard, pk=kwargs['pk'])
        if not self.orchard.can_edit(self.request.user):
            raise PermissionDenied
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse('orchard', args=[self.object.orchard.pk])

    def get_form(self):
        form = super().get_form()
        if self.request.method in ('POST', 'PUT'):
            self.photo_fset = PhotosFormSet(self.request.POST, self.request.FILES, instance=form.instance)
        else:
            self.photo_fset = PhotosFormSet(instance=form.instance)
        return form

    def form_valid(self, form):
        if self.photo_fset.is_valid():
            self.object = form.save()
            self.photo_fset.instance = self.object
            self.photo_fset.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [
                (_('Home'), reverse('home')),
                (self.orchard.name, reverse('orchard', args=[self.orchard.pk])),
            ],
            'title': _("Editing an offer"),
            'photosformset': self.photo_fset,
        })
        return context


class ProductNewView(ProductEditBaseMixin, CreateView):
    mode = 'add'
    success_message = _("%(title)s was created successfully")

    def get_initial(self):
        initial = super().get_initial()
        initial['orchard'] = self.orchard
        return initial


class ProductEditView(ProductEditBaseMixin, UpdateView):
    mode = 'edit'
    pk_url_kwarg = 'pk_product'
    success_message = _("%(title)s was changed successfully")


class ProductDeleteView(DeleteView):
    pk_url_kwarg = 'pk_product'
    model = Product

    def dispatch(self, *args, **kwargs):
        self.orchard = get_object_or_404(Orchard, pk=kwargs['pk'])
        if not self.orchard.can_edit(self.request.user):
            raise PermissionDenied
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse('orchard', args=[self.kwargs['pk']])

    def form_valid(self, form):
        product = self.object
        if product.productorder_set.filter(status='ordered').count() > 0:
            messages.error(
                self.request,
                'There are pending orders for this product. You have to cancel or finish them '
                'before you can delete your account.'
            )
            return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', "/"))
        # Archive product orders
        for order in product.productorder_set.all():
            order.archive(reason='Product owner deleted the product')
        response = super().form_valid(form)
        messages.success(self.request,  _("%(title)s was deleted successfully") % {'title': product.title })
        return response


class ProductListView(TitleViewMixin, ListView):
    model = Product
    template_name = 'product_list.html'
    title = _("Offer list")

    def get_queryset(self):
        def mark(prod):
            prod.avail_dates = prod.next_availability_dates() or ''
            return prod

        def sort_key(prod):
            return (prod.typ, prod.avail_dates[0] if prod.avail_dates else date(2050, 1, 1))

        return sorted([
            mark(prod) for prod in super().get_queryset(
                ).exclude(availability=False).exclude(orchard__is_public=False
                ).prefetch_related('orchard__trees', 'fruit_species')
            ], key=sort_key
        )


class ProductOwnView(LoginRequiredMixin, TitleViewMixin, ListView):
    """List of 'my products'."""
    model = Product
    template_name = 'product_list.html'
    title = _("My Offers")

    def get_queryset(self):
        return super().get_queryset().filter(orchard__owner=self.request.user).order_by('title')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['editable'] = True
        return context


class OrchardOwnView(LoginRequiredMixin, TitleViewMixin, ListView):
    """List of 'my orchards'."""
    model = Orchard
    template_name = 'orchard_list.html'
    title = _("My Orchards")

    def get_queryset(self):
        return super().get_queryset().filter(owner=self.request.user).order_by('name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['editable'] = True
        return context


class ProductDetailView(DetailView):
    model = Product
    pk_url_kwarg = 'pk_product'
    template_name = 'product_detail.html'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            if self.request.user != form.instance.user:
                raise PermissionDenied("Ordering for another user is forbidden.")
            order = form.save()
            messages.success(self.request, _(
                "Your order has been sucessfully received. The product owner "
                "will now review and confirm your order during the next days. "
                "You will then receive the payment information by email.")
            )
            # Send email to product owner, asking for confirmation.
            with override(order.product.orchard.owner.language):
                hostname = _("meinobstgarten.ch")
                orchard_contact = order.product.orchard.get_contact()
                body = loader.render_to_string('email/product_order.txt', {
                    'hostname': hostname,
                    'product': order.product,
                    'orchard': order.product.orchard,
                    'user': order.user,
                    'quantity': order.quantity,
                    'price': order.total_price,
                    'confirm_url': reverse('productorder_detail', args=[order.product.orchard.pk, order.pk]),
                }, using='django-text')
                send_mail(
                    _('[{host}] Product order {id}').format(host=hostname, id=order.order_id),
                    body,
                    settings.SITE_EMAIL,
                    [orchard_contact['email']],
                )
            return HttpResponseRedirect(self.request.path)
        else:
            return self.render_to_response(self.get_context_data(order_form=form))

    def get_form(self):
        data = self.request.POST if self.request.method == 'POST' else None
        return ProductOrderForm(
            initial={'product': self.object, 'user': self.request.user},
            data=data,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [
                (_('Home'), reverse('home')),
                (self.object.orchard.name, reverse('orchard', args=[self.object.orchard.pk])),
            ],
            'title': self.object.title,
            'can_order': self.object.availability and self.request.user.is_authenticated,
            'can_edit': self.object.orchard.owner == self.request.user,
            'trees': self.object.target_trees(),
            'avail_dates': self.object.next_availability_dates(),
        })
        if context['can_order'] and not 'order_form' in context:
            context['order_form'] = self.get_form()
        return context


class ProductOrderDetailView(DetailView):
    model = ProductOrder
    pk_url_kwarg = 'pk_order'
    template_name = 'productorder_detail.html'

    def dispatch(self, *args, **kwargs):
        self.order = get_object_or_404(ProductOrder, pk=kwargs['pk_order'])
        if not self.order.can_view(self.request.user):
            raise PermissionDenied
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        if self.request.POST.get('step') == 'confirm':
            confirm_form = ProductOrderConfirmForm(data=self.request.POST)
            if confirm_form.is_valid():
                self.confirm_order(confirm_form)
        elif self.request.POST.get('step') == 'sent':
            self.order.send_date = timezone.now()
            self.order.status = 'sent'
            self.order.save()
            messages.success(request, _(
                "The product has been marked as sent in our database. Thanks for "
                "using the meinobstgarten.ch platform.")
            )
        elif self.request.POST.get('step') == 'cancel':
            self.cancel_order()

        return HttpResponseRedirect(self.request.path)

    def confirm_order(self, form):
        self.order.confirm_date = timezone.now()
        self.order.status = 'confirmed'
        self.order.save()
        messages.success(self.request, _(
            "Thanks for the confirmation. An email was sent to the client "
            "with your paiement information.")
        )
        # Send email to client
        with override(self.order.user.language):
            hostname = _("meinobstgarten.ch")
            orchard_contact = self.order.product.orchard.get_contact()
            body = loader.render_to_string(self.order.product.get_confirmation_template(), {
                'hostname': hostname,
                'product': self.order.product,
                'orchard': self.order.product.orchard,
                'order': self.order,
                'paiement_data': orchard_contact['paiement_info'],
                'remark': form.cleaned_data['remark'].strip(),
            }, using='django-text')
            send_mail(
                _('[{host}] Product order {id}').format(host=hostname, id=self.order.order_id),
                body,
                settings.SITE_EMAIL,
                [self.order.user.email],
            )

    def cancel_order(self):
        self.order.cancel_date = timezone.now()
        self.order.status = 'cancelled'
        self.order.cancel_reason = self.request.POST.get('cancel_reason')
        self.order.save()
        messages.success(self.request, _(
            "The product order has been cancelled and an email was sent to "
            "both parties.")
        )
        # Send email to both
        def send_cancel_email(email):
            hostname = _("meinobstgarten.ch")
            body = loader.render_to_string('email/product_order_cancel.txt', {
                'hostname': hostname,
                'product': self.order.product,
                'order': self.order,
                'reason': self.order.cancel_reason,
            }, using='django-text')
            send_mail(
                _('[{host}] Product order {id} cancelled').format(
                    host=hostname, id=self.order.order_id),
                body,
                settings.SITE_EMAIL,
                [email],
            )
        with override(self.order.user.language):
            send_cancel_email(self.order.user.email)

        with override(self.order.product.orchard.owner.language):
            send_cancel_email(self.order.product.orchard.get_contact()['email'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [
                (_('Home'), reverse('home')),
                (self.order.product.orchard.name, reverse('orchard', args=[self.order.product.orchard.pk])),
                (self.order.product.title, reverse(
                    'product_detail', args=[self.order.product.orchard.pk, self.order.product.pk])
                ),
            ],
            'title': _("Product Order"),
            'confirm_form': ProductOrderConfirmForm(),
        })
        if (self.order.status == 'ordered' and
                (self.request.user == self.order.product.orchard.owner or self.request.user.is_superuser)):
            context['step'] = 'confirm'
        elif self.order.status == 'confirmed' and self.request.user == self.order.product.orchard.owner:
            context['step'] = 'mark_sent'
        return context

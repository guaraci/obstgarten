from django.conf import settings
from django.core.mail import EmailMessage
from django.core.mail.backends.smtp import EmailBackend


def send_mail(subject, message, from_email, recipient_list, **kwargs):
    connection = None
    if not from_email:
        from_email = settings.SERVER_EMAIL
    domain = from_email.rsplit('@')[-1]
    if domain in settings.SMTP_CONNECTIONS:
        data = settings.SMTP_CONNECTIONS[domain]
        connection = EmailBackend(
            host=data['EMAIL_HOST'],
            port=data['EMAIL_PORT'],
            username=data['EMAIL_HOST_USER'],
            password=data['EMAIL_HOST_PASSWORD'],
            use_ssl=data['EMAIL_USE_SSL'],
            use_tls=data['EMAIL_USE_TLS'],
        )
    msg = EmailMessage(
        subject, message, from_email, recipient_list, connection=connection, **kwargs
    )
    msg.send()

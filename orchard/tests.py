import re
from datetime import date, timedelta
from decimal import Decimal
from html import escape

from django.conf import settings
from django.contrib.gis.geos import Point
from django.core import mail
from django.test import TestCase, override_settings
from django.urls import reverse
from django.utils import timezone
from django.utils.formats import date_format
from django.utils.html import escape
from django.utils.translation import gettext as _

from accounts.models import User
from locations.models import Location
from .forms import ProductOrderForm, SearchForm
from .management.commands.dailytasks import Command as DailyTasksCommand
from .models import (
    Booking, BookingArchived, FruitCategory, Orchard, Product, ProductOrder,
    ProductOrderArchived, SavedSearch, Tree, TreeSpecies, YearState,
)


class TestDataMixin:
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user = User.objects.create_user(
            first_name='Aron', last_name='Apfelbaum', street='Obstweg 23', pcode='5555',
            city='Apfelmus', email="test@example.org", password='abcdef',
            phone='099 444 55 66', paiement_info='CCP 25-25252525-0', language='en',
        )
        cls.category = FruitCategory.objects.create(name='pome fruits')
        cls.species = TreeSpecies.objects.create(name="pear tree", fruit="pear", category=cls.category)
        cls.location = Location.objects.create(name='Hofstetten-Flüh', center=Point(2605250, 1257934, srid=2056))
        cls.orchard = Orchard.objects.create(
            owner=cls.user, name="Test orchard", municipality=cls.location, is_public=True,
            pick_details="<p>A ladder is available.</p>",
        )
        cls.previous_year = timezone.now().year - 1
        cls.next_year = timezone.now().year + 1

    @classmethod
    def add_tree(cls, orchard, **kwargs):
        data = dict(orchard=orchard, species=cls.species,
            point=Point(600000,300000, srid=2056),
            pick_start='09.01',
            pick_end='09.15',
        )
        data.update(**kwargs)
        return Tree.objects.create(**data)

    def assertMessage(self, response, msg):
        self.assertEqual(msg, ''.join([str(m) for m in response.context['messages']]))


@override_settings(LANGUAGE_CODE='en')
class OrchardTests(TestDataMixin, TestCase):
    def test_anonymous_cannot_create(self):
        response = self.client.get(reverse('orchard_new'))
        self.assertRedirects(response, reverse('login') + '?next=%s' % reverse('orchard_new'))

    def test_orchard_create(self):
        self.client.login(email=self.user.email, password='abcdef')
        response = self.client.post(reverse('orchard_new'), data={
            'name': 'Test create orchard',
            'description': 'Some descr.',
            'municipality': self.location.name,
            'is_public': 'on'
        })
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        orch = Orchard.objects.get(name='Test create orchard')
        self.assertRedirects(response, orch.get_absolute_url())
        response = self.client.get(orch.get_absolute_url())
        self.assertContains(response, _("Add a tree"))
        # An available tree to have the booking form
        tree = self.add_tree(orch, availability='whole', pick_start='12.15', pick_end='12.31')
        response = self.client.get(orch.get_absolute_url())
        self.assertContains(
            response,
            '<div class="booking-form" data-url="/tree/%s/%s/book/"></div>' % (tree.pk, date.today().year),
            html=True,
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [settings.SITE_EMAIL])
        self.assertEqual(
            mail.outbox[0].body,
            'The following new orchard was created by Aron Apfelbaum:\n'
            'https://www.meinobstgarten.ch/orchard/%s/' % orch.pk
        )

    def test_orchard_private(self):
        orchard = Orchard.objects.create(
            owner=self.user, name="Test private", municipality=self.location, is_public=False,
        )
        response = self.client.get(reverse('orchard', args=[orchard.pk]))
        self.assertEqual(response.status_code, 403)
        self.client.login(email=self.user.email, password='abcdef')
        response = self.client.get(reverse('orchard', args=[orchard.pk]))
        self.assertContains(response, 'Test private')

    def test_map_view(self):
        private_orchard = Orchard.objects.create(
            owner=self.user, name="Test private orchard", is_public=False,
            municipality=self.location
        )
        self.add_tree(self.orchard)
        self.add_tree(private_orchard)
        response = self.client.get(reverse('map'))
        # Only one is publicly visible
        self.assertEqual(len(response.context['orchards']), 1)

        self.client.force_login(self.user)
        response = self.client.get(reverse('map'))
        # Both are now visible (owner is logged in)
        self.assertEqual(len(response.context['orchards']), 2)

    def test_orchard_edit(self):
        edit_url = reverse('orchard_edit', args=[self.orchard.pk])
        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 403)
        self.client.login(email="test@example.org", password='abcdef')
        response = self.client.get(edit_url)
        self.assertContains(response,
            '<input class="input-group__field" type="text" name="municipality" value="%s" id="id_municipality" required>' % self.location.name,
            html=True
        )
        self.assertContains(response, 'class="input-group__field tinymce"')
        self.assertContains(response,
            '<input id="id_name" class="input-group__field" maxlength="100" name="name" '
            'type="text" value="Test orchard" required>',
            html=True
        )
        post_data = {
            'name': "Test orchard",
            'municipality': self.location.name,
            'description': "<p>Some description</p>",
            'is_public': "on",
            'i_am_owner': 'True',
            'orchard-photo-content_type-object_id-TOTAL_FORMS': "1",
            'orchard-photo-content_type-object_id-INITIAL_FORMS': "0",
        }
        response = self.client.post(edit_url, data=post_data)
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('orchard', args=[self.orchard.pk]))
        # Test with wrong municipality name
        post_data['municipality'] = 'test'
        response = self.client.post(edit_url, data=post_data)
        self.assertContains(response, escape(_("'%s' is not a recognized Swiss municipality.") % 'test'))

    def test_send_message_to_owner(self):
        user = User.objects.create_user(email="client2@example.org")
        # A product must exist to have the contact form.
        Product.objects.create(orchard=self.orchard, title="My product", availability=True)
        self.client.force_login(user)
        response = self.client.get(reverse('orchard', args=[self.orchard.pk]))
        self.assertContains(
            response,
            '<textarea class="input-group__field" name="message" cols="40" rows="10" id="id_message" required></textarea>',
            html=True
        )
        message = "This is a message\nfrom a user!"
        response = self.client.post(
            reverse('orchard-sendmail', args=[self.orchard.pk]),
            data={'message': message},
            follow=True,
        )
        self.assertContains(
            response,
            _("Your message was successfully sent to the orchard owner.")
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), [self.orchard.owner.email])
        self.assertIn(message, mail.outbox[0].body)

    def test_orchard_delete(self):
        orchard = Orchard.objects.create(
            owner=self.user, name="Test orchard", is_public=True, municipality=self.location
        )
        # Create prouct, order and bookings for this orchard
        user = User.objects.create_user('jane@example.org', password='sghz%er*')
        product = Product.objects.create(
            orchard=orchard, title="My product", availability=True, price='8.45',
            sending_costs='2.5',
        )
        order = ProductOrder.objects.create(
            user=user, product=product, total_price='8.45', order_date=timezone.now(),
            status='ordered'
        )
        tree = self.add_tree(orchard, variety='Sülibirne')
        booking = Booking.objects.create(tree=tree, year=self.next_year, user=user, status='planned')

        response = self.client.get(reverse('orchard_delete', args=[orchard.pk]))
        self.assertEqual(response.status_code, 403)
        response = self.client.post(reverse('orchard_delete', args=[orchard.pk]))
        self.assertEqual(response.status_code, 403)
        self.client.login(email="test@example.org", password='abcdef')
        response = self.client.post(reverse('orchard_delete', args=[orchard.pk]), follow=True)
        self.assertMessage(
            response,
            'You cannot delete the orchard while it has pending product orders and bookings. '
            'Cancel them before you try again.'
        )
        booking.status = 'cancelled'
        booking.save()
        order.status = 'cancelled'
        order.save()
        booking_count = Booking.objects.count()
        order_count = ProductOrder.objects.count()
        orchard_pk = orchard.pk
        response = self.client.get(reverse('orchard_delete', args=[orchard.pk]))
        # Delete confirmation page
        self.assertContains(response, 'Yes, delete this orchard')
        response = self.client.post(reverse('orchard_delete', args=[orchard.pk]), follow=True)
        self.assertMessage(response, _('The orchard "%s" has been deleted.') % "Test orchard")
        self.assertEqual(Booking.objects.count(), booking_count - 1)
        self.assertEqual(BookingArchived.objects.filter(tree__orchard__pk=orchard_pk).count(), 1)
        self.assertEqual(ProductOrder.objects.count(), order_count - 1)
        self.assertEqual(ProductOrderArchived.objects.filter(product__orchard__pk=orchard_pk).count(), 1)

    def test_tree_new(self):
        self.client.login(email="test@example.org", password='abcdef')
        response = self.client.post(reverse('tree_new', args=[self.orchard.pk]), data={
            'orchard': str(self.orchard.pk),
            'species': str(self.species.pk),
            'variety': "Apple",
            'variety_descr': "",
            'general_descr': "",
            'point': 'POINT (559199.996948242 205416.625976571)',
            'availability': 'whole',
            'yearstate_set-INITIAL_FORMS': '0',
            'yearstate_set-TOTAL_FORMS': '0',
            'orchard-photo-content_type-object_id-TOTAL_FORMS': '1',
            'orchard-photo-content_type-object_id-INITIAL_FORMS': '0',
        })
        self.assertRedirects(response, reverse('orchard', args=[self.orchard.pk]))
        tree = self.orchard.trees.filter(variety='Apple').first()
        self.assertEqual(tree.species, self.species)

    def test_tree_edit(self):
        self.client.login(email="test@example.org", password='abcdef')
        tree = self.add_tree(self.orchard, variety='Sülibirne')
        edit_url = reverse('tree_edit', args=[self.orchard.pk, tree.pk])
        post_data = {
            'orchard': str(tree.orchard.pk),
            'species': str(tree.species.pk),
            'variety': 'Sülibirne',
            'variety_descr': '<p>Die S&uuml;libirne ist eine kleine s&uuml;sse Birne.</p>',
            'general_descr': '<p>Mit der S&uuml;libirne kann ein ausgezeichneter Schnaps hergestellt werden.</p>',
            'point': 'POINT (559199.996948242 205416.625976571)',
            'availability': 'whole',
            'usages': ['dried', 'preserve'],
            'yearstate_set-INITIAL_FORMS': '0',
            'yearstate_set-TOTAL_FORMS': '1',
            'yearstate_set-0-year': date.today().year,
            'yearstate_set-0-fruit_quant': '10-50kg',
            'orchard-photo-content_type-object_id-TOTAL_FORMS': '1',
            'orchard-photo-content_type-object_id-INITIAL_FORMS': '0',
        }
        response = self.client.post(edit_url, data=post_data)
        if response.status_code == 200 and not response.context['form'].is_valid():
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, reverse('orchard', args=[tree.orchard.pk]))
        tree.refresh_from_db()
        self.assertEqual(tree.usages, ['dried', 'preserve'])

        # When setting availability as self-picking, create a self-picking product if none exist.
        post_data['availability'] = 'picking'
        response = self.client.post(edit_url, data=post_data, follow=True)
        pick_prods = self.orchard.products.filter(typ='self')
        self.assertEqual(pick_prods.count(), 1)
        self.assertQuerySetEqual(pick_prods.first().fruit_species.all(), [tree.species])
        self.assertContains(
            response,
            '<p class="message is-warning">A new self-picking product has been automatically '
            'created. Please <a href="%s">edit its details</a> now!</p>' % reverse(
                'product_edit', args=[self.orchard.pk, pick_prods.first().pk]
            )
        )
        # If another tree exists with the same variety name, the app suggests a copy
        # of changed fields
        tree2 = self.add_tree(self.orchard, variety='Sülibirne')
        post_data.update({
            'variety_descr': '<p>Die S&uuml;libirne ist eine Birne.</p>',
            'pick_start': '08.15',
            'pick_end': '08.31',
            'general_descr': 'Nothing special',
        })
        response = self.client.post(edit_url, data=post_data)
        self.assertTrue(response.url.endswith(
            'copymodifs/?fields=variety_descr,pick_start,pick_end&next=/orchard/%d/' % self.orchard.pk
        ))

    def test_tree_copy_modifs(self):
        tree1 = self.add_tree(
            self.orchard, variety='Sülibirne',
            variety_descr='<p>Die S&uuml;libirne ist eine Birne.</p>',
            usages=['dessert', 'juice'],
        )
        tree2 = self.add_tree(self.orchard, variety='Sülibirne')
        self.client.force_login(self.user)
        url = reverse('tree_copymodifs', args=[self.orchard.pk, tree1.pk]) + '?fields=variety_descr,usages&next=/'
        response = self.client.get(url)
        self.assertEqual(
            set(response.context['form'].fields.keys()),
            {'usages', 'variety_descr', 'next'}
        )
        response = self.client.post(url, data={'yes': 'yes', 'variety_descr': 'on', 'next': '/'})
        if response.status_code == 200:
            self.fail(response.context['form'].errors)
        self.assertRedirects(response, '/')
        tree2.refresh_from_db()
        self.assertEqual(tree1.variety_descr, tree2.variety_descr)

    def test_tree_duplicate(self):
        tree = self.add_tree(self.orchard)
        self.client.force_login(self.user)
        response = self.client.get(reverse('tree_dup', args=[self.orchard.pk, tree.pk]))
        self.assertIsNone(response.context['form'].instance.pk)
        self.assertEqual(response.context['form'].instance.pick_start, '09.01')
        self.assertIsNone(response.context['form'].instance.point)
        # The map should be centered on the orchard (small resolution)
        self.assertContains(response, 'resolution: 0.5')

    def test_tree_delete(self):
        tree = self.add_tree(self.orchard)
        self.client.login(email=self.user.email, password='abcdef')
        response = self.client.post(
            reverse('tree_delete', args=[self.orchard.pk, tree.pk]), data={}
        )
        self.assertRedirects(response, self.orchard.get_absolute_url())
        self.assertEqual(self.orchard.trees.count(), 0)

    def _create_booking_data(self, price=None):
        tree = self.add_tree(self.orchard, availability='whole')
        if price:
            tree.price = price
            tree.pay_method = ['adv', 'cash']
            tree.save()

        # Only to have a fruit_quantity
        YearState.objects.create(
            tree=tree, year=self.next_year, fruit_quant='>200kg',
        )
        user1 = User.objects.create_user(
            first_name="Nora", last_name="Schmid", street="Baumstrasse 1", pcode="8888",
            city="Obstwil", phone="053 1114423",
            email="client1@example.org", password='client',
        )
        return tree, user1

    def test_tree_booking(self):
        tree, user1 = self._create_booking_data(price=10)
        self.client.force_login(user1)
        response = self.client.post(reverse('tree-book', args=[tree.pk, self.next_year]), data={}, follow=True)
        self.assertRedirects(response, reverse('bookings-list', args=[user1.pk]))
        self.assertEqual(user1.booking_set.count(), 1)
        # Test mail sent
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].recipients(), ['client1@example.org'])
        self.assertEqual(
            mail.outbox[0].body,
            '''This is a confirmation email sent from the www.meinobstgarten.ch platform.

You just booked the fruit collection for the tree  (pear) in orchard "Test orchard".
Quantity: >200kg
Collection period: 01.09 – 15.09.{next_year}
Price: 10 CHF
Paiement method: Advance paiement, Cash on site
https://www.meinobstgarten.ch/orchard/{pk}/tree/{pk_tree}/

Please contact the orchard responsible person at least one week before to inform about the exact data you plan to go and collect fruits:

Aron Apfelbaum
Obstweg 23
5555 Apfelmus
Phone 099 444 55 66
Email test@example.org
Paiement information:
CCP 25-25252525-0

Pick instructions
=================
A ladder is available.

WARNING: Only use a ladder if you are confident and have the necessary knowledge and experience. For the right position of a ladder, please read our leaflet.
https://www.meinobstgarten.ch/media/Sicherheitsmerkblatt.pdf

Enjoy the fruits!

The meinobstgarten.ch team.
'''.format(pk=tree.orchard.pk, pk_tree=tree.pk, next_year=self.next_year)
        )
        self.assertEqual(mail.outbox[1].recipients(), [self.user.email])
        self.assertEqual(
            mail.outbox[1].body,
            '''The following person booked fruit collection for the tree  (pear) in your orchard Test orchard on the site www.meinobstgarten.ch.
https://www.meinobstgarten.ch/orchard/{pk}/tree/{pk_tree}/


Nora Schmid
Baumstrasse 1
8888 Obstwil
Phone 053 1114423
Email client1@example.org

This person was asked to inform you at least one week before the collection date about the exact collection date.

Thanks for sharing your property!

The meinobstgarten.ch team.
'''.format(pk=tree.orchard.pk, pk_tree=tree.pk)
        )
        # Test parallel bookings are not possible
        user2 = User.objects.create_user(email="client2@example.org", password='client')
        self.client.logout()
        self.client.login(email=user2.email, password='client')
        response = self.client.post(reverse('tree-book', args=[tree.pk, self.next_year]), data={}, follow=True)
        self.assertContains(response, _("Sorry, someone booked that tree before you!"))

    def test_tree_booking_cancel(self):
        tree, user1 = self._create_booking_data()
        booking = Booking.objects.create(tree=tree, year=self.next_year, user=user1, status='planned')
        self.client.login(email=user1.email, password='client')
        response = self.client.post(reverse('booking-delete'), data={'bookingid': booking.pk})
        self.assertRedirects(response, reverse('bookings-list', args=[user1.pk]))
        self.assertEqual(tree.booking_set.count(), 0)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].body,
            '''The fruit collection booking for the tree  (pear) in your orchard Test orchard on the site www.meinobstgarten.ch was cancelled by Nora Schmid.

The meinobstgarten.ch team.
'''
        )

    def test_tree_booking_list(self):
        tree, user1 = self._create_booking_data(price=10)
        booking = Booking.objects.create(tree=tree, year=self.next_year, user=user1, status='planned')
        self.client.force_login(user1)
        response = self.client.get(reverse('bookings-list', args=[user1.pk]))
        self.assertContains(
            response, '(pear) in orchard <a href="/orchard/%d/">Test orchard</a>.' % self.orchard.pk
        )
        self.assertContains(
            response, '&gt;200kg (01.09 – 15.09.%s) , 10 CHF' % self.next_year, html=True
        )

    def test_tree_booking_availability(self):
        tree = self.add_tree(self.orchard, availability='none', pick_start='', pick_end='')
        self.assertFalse(tree.is_available_now)
        self.assertFalse(tree.booking_available(self.next_year))
        tree.availability = 'whole'
        tree.save()
        self.assertFalse(tree.booking_available(self.next_year))
        # pick dates in the past
        tree.pick_start = '01.01'
        tree.pick_end = '01.02'
        tree.save()
        self.assertTrue(tree.is_passed(self.previous_year))
        self.assertFalse(tree.is_available_now)

        self.assertTrue(tree.booking_available(self.next_year))
        # booked? then no longer available
        user = User.objects.create_user(email="client@example.org")
        bk = Booking.objects.create(tree=tree, year=self.next_year, user=user, status='planned')
        self.assertFalse(tree.booking_available(self.next_year))
        # Same test with prefetch cache filled (other code path)
        self.assertFalse(
            Tree.objects.filter(pk=tree.pk).prefetch_related('booking_set')[0].booking_available(self.next_year)
        )
        self.assertFalse(tree.is_available_now)
        # book cancelled, again available
        bk.status = 'cancelled'
        bk.save()
        self.assertTrue(tree.booking_available(self.next_year))

    def test_tree_picking_availability(self):
        tree = self.add_tree(self.orchard, availability='picking', pick_start='', pick_end='')
        self.assertFalse(tree.booking_available(self.next_year))
        self.assertFalse(tree.picking_available(self.next_year))
        tree.pick_start = '12.15'
        tree.pick_end = '12.31'
        tree.save()
        self.assertTrue(tree.picking_available(self.next_year))


class TreeSearchTests(TestDataMixin, TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # Coordinates not far from Hofstetten-Flüh
        cls.hf_coords = [2606100, 1258100]
        other_cat = FruitCategory.objects.create(name='Apple')
        other_species = TreeSpecies.objects.create(name="Golden", category=other_cat)
        cls.add_tree(
            cls.orchard, variety='special1', point=Point(*cls.hf_coords, srid=2056),
            availability='whole', pick_start='', pick_end='',
        )
        cls.add_tree(
            cls.orchard, variety='special2',
            point=Point(cls.hf_coords[0] + 5, cls.hf_coords[1] - 5, srid=2056),
            availability='whole', pick_start='', pick_end='',
        )
        cls.add_tree(
            cls.orchard, species=other_species,
            point=Point(cls.hf_coords[0] + 15, cls.hf_coords[1] - 15, srid=2056),
            availability='picking', pick_start='', pick_end='',
        )
        # Location outside the Hofstetten-Flüh neighbourhood
        cls.add_tree(
            cls.orchard,
            point=Point(2640000, 1230000, srid=2056)
        )

    def _set_tree_available(self, tree):
        """Set pick_start/pick_end to be available independently of current time."""
        start = timezone.now() + timedelta(days=90)
        tree.pick_start = (
            '{:02}.{:02}'.format(start.month, start.day)
            if start.year == timezone.now().year else '12.01'
        )
        tree.pick_end = '12.31'
        tree.save()

    def test_unbound_search(self):
        # Unbound form
        response = self.client.get(reverse('collect'))
        self.assertContains(response, '<form')

    def test_search_all_categs(self):
        # Blank category (all fruits)
        form = SearchForm(data={
            'from_loc': 'Hofstetten-Flüh',
            'radius': '10',
            'category': '',
        })
        self.assertTrue(form.is_valid())
        self.assertEqual(len(form.search()[0]), 3)
        self.assertEqual(form.name_from_data(), 'All fruits - Hofstetten-Flüh (10 km)')

    def test_search(self):
        search_data = {
            'category': self.species.pk,
            'from_loc': 'Hofstetten-Flüh',
            'radius': '10',
            'only_avail': '',
        }
        form = SearchForm(data=search_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(len(form.search()[0]), 2)
        self.assertEqual(form.name_from_data(), 'pear - Hofstetten-Flüh (10 km)')
        self.client.force_login(self.user)
        response = self.client.get(
            reverse('collect') + '?category=&from_loc=Hofstetten-Flüh&radius=15'
        )
        # All trees in same orchard
        self.assertEqual(int(response.context['center'][0]), 605250)
        self.assertEqual(int(response.context['center'][1]), 257933)
        self.assertContains(response, 'Your search returned 1 orchard')

    def test_search_typ_only(self):
        search_data = {
            'typ': ['tree', 'self'],
            'category': '',
            'from_loc': '',
            'radius': '',
            'only_avail': '',
        }
        form = SearchForm(data=search_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(len(form.search()[0]), 3)

    def test_search_only_avail(self):
        # No real available trees
        search_data = {
            'category': self.species.pk,
            'from_loc': 'Hofstetten-Flüh',
            'radius': '10',
            'only_avail': 'on',
        }
        form = SearchForm(data=search_data)
        self.assertTrue(form.is_valid())
        self.assertEqual(len(form.search()[0]), 0)
        # tree1 is available
        self._set_tree_available(Tree.objects.get(variety='special1'))
        # tree2 is not available
        tree2 = Tree.objects.get(variety='special2')
        tree2.pick_start = '01.01'
        tree2.pick_end = '01.15'
        tree2.availability = 'none'
        tree2.save()
        self.assertQuerySetEqual(form.search()[0], ['<Tree: special1 (pear)>'], transform=repr)

    def test_search_save(self):
        # At least one tree must exist to appear in fruit select
        Tree.objects.create(
            orchard=self.orchard, species=self.species, point=Point(600000,300000, srid=2056)
        )
        self.client.login(email=self.user.email, password='abcdef')
        response = self.client.get(
            reverse('collect') + '?category=%d&from_loc=Hofstetten-Flüh&radius=15' % (
                self.species.pk)
        )
        self.assertIn('save_form', response.context)
        response = self.client.post(reverse('search-save'), data={
            'form_data': "from_loc=Hofstetten-Flüh&radius=15&category=%d" % self.species.pk,
            'name': "pear - Hofstetten-Flüh (15 km)",
            'interval': 'weekly',
        })
        self.assertRedirects(response, reverse('savedsearch-list', args=[self.user.pk]))
        saveds = SavedSearch.objects.filter(name='pear - Hofstetten-Flüh (15 km)')
        self.assertEqual(saveds.count(), 1)
        self.assertEqual(
            eval(saveds[0].form_data),
            {'category': str(self.species.pk), 'from_loc': 'Hofstetten-Flüh', 'radius': '15'}
        )
        # Empty form data
        response = self.client.post(reverse('search-save'), data={
            'form_data': "",
            'name': "pear - Hofstetten-Flüh (15 km)",
            'interval': 'weekly',
        }, follow=True)
        self.assertContains(response, _("Sorry, something went wrong with your request"))

    def test_savedsearch_interval_past(self):
        saveds = SavedSearch(
            user=self.user, name='Pomme - Basel (15km)', interval='weekly', form_data=''
        )
        self.assertTrue(saveds.interval_past())
        saveds.last_sent = timezone.now()
        self.assertFalse(saveds.interval_past())
        saveds.last_sent = timezone.now() - timedelta(days=7)
        self.assertTrue(saveds.interval_past())
        saveds.interval = 'daily'
        self.assertTrue(saveds.interval_past())

    def test_savedsearch_new_trees(self):
        saveds = SavedSearch(
            user=self.user, name='Pomme - Basel (15km)', interval='weekly',
            form_data="{'from_loc': 'Hofstetten-Flüh', 'radius': 5, 'category': %d}" % self.species.pk,
        )
        self.assertEqual(len(saveds.new_trees()), 0)
        # Add a new tree
        orchard = Orchard.objects.create(
            owner=self.user, name="Test orchard", is_public=True, municipality=self.location
        )
        tree = self.add_tree(
            orchard, variety="Poirier sauvage",
            availability='whole',
            point=Point(self.hf_coords[0] - 23, self.hf_coords[1] + 33, srid=2056)
        )
        self._set_tree_available(tree)
        self.assertEqual(len(saveds.new_trees()), 1)
        saveds.last_tree_list = str(tree.pk)
        self.assertEqual(len(saveds.new_trees()), 0)

    def test_savedsearch_periodic_mail(self):
        saveds = SavedSearch.objects.create(
            user=self.user, name='pomme - Hofstetten (5km)', interval='weekly',
            form_data="{'from_loc': 'Hofstetten-Flüh', 'radius': 5, 'category': %d}" % self.species.pk,
        )
        DailyTasksCommand().send_new_search_results()
        # No trees, no mail
        self.assertEqual(len(mail.outbox), 0)
        # Add a new tree
        orchard = Orchard.objects.create(
            owner=self.user, name="Test orchard", is_public=True, municipality=self.location
        )
        tree = Tree.objects.create(
            orchard=orchard, species=self.species, variety="Poirier sauvage",
            point=Point(2606077, 1258133, srid=2056),
            availability='whole',
        )
        self._set_tree_available(tree)
        DailyTasksCommand().send_new_search_results()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].body,
'''This is the regular email informing you about new trees available from your saved search query on the www.meinobstgarten.ch website.

Your search: https://www.meinobstgarten.ch/collect/?category={cat_pk}&from_loc=Hofstetten-Fl%C3%BCh&radius=5

New trees
---------
 * Poirier sauvage (pear) in Test orchard
   https://www.meinobstgarten.ch/orchard/{orchard_pk}/



If you don't want to receive new email for your search, use the link at the top and delete your search.
'''.format(cat_pk=self.species.pk, orchard_pk=orchard.pk))
        saveds.refresh_from_db()
        self.assertIsNotNone(saveds.last_sent)
        self.assertEqual(saveds.last_tree_list, str(tree.pk))
        # No new mail this second time
        DailyTasksCommand().send_new_search_results()
        self.assertEqual(len(mail.outbox), 1)
        # Duplicate tree and check last_tree_list
        tree1_pk = tree.pk
        tree.pk = None
        tree.save()
        saveds.last_sent = None
        saveds.save()
        DailyTasksCommand().send_new_search_results()
        self.assertEqual(len(mail.outbox), 2)
        saveds.refresh_from_db()
        self.assertEqual(saveds.last_tree_list, '%d,%d' % (tree1_pk, tree.pk))


@override_settings(LANGUAGE_CODE='en')
class ProductTests(TestDataMixin, TestCase):
    def test_product_new(self):
        self.client.force_login(self.user)
        response = self.client.post(
            reverse('product_new', args=[self.orchard.pk]),
            data={
                'title': "A new product",
                'orchard': str(self.orchard.pk),
                'typ': 'prod',
                'confirm_conditions': 'on',
                'orchard-photo-content_type-object_id-INITIAL_FORMS': "0",
                'orchard-photo-content_type-object_id-TOTAL_FORMS': "1",
            }
        )
        self.assertRedirects(response, reverse('orchard', args=[self.orchard.pk]))
        self.assertEqual(Product.objects.get(title="A new product").orchard, self.orchard)

    def test_delete_product(self):
        product = Product.objects.create(
            title='Self-picking fruit', orchard=self.orchard,
            typ='self',
        )
        product.fruit_species.add(self.species)
        client = User.objects.create_user(
            first_name='Nora', last_name='Schmied', street='Apfelstr. 11', pcode='5544',
            city='Birnheim', email="client@example.org", password='123456',
        )
        order = ProductOrder.objects.create(
            user=client, product=product, quantity=1, total_price=20,
            order_date=timezone.now(), status='ordered',
        )
        self.client.force_login(self.user)
        delete_url = reverse('product_delete', args=[self.orchard.pk, product.pk])
        response = self.client.post(delete_url, follow=True)
        self.assertContains(
            response,
            'There are pending orders for this product. You have to cancel or finish them '
            'before you can delete your account.'
        )
        order.status = 'cancelled'
        order.save()
        order_count = ProductOrder.objects.count()
        response = self.client.post(delete_url, follow=True)
        self.assertContains(response, "Self-picking fruit was deleted successfully")
        self.assertEqual(ProductOrder.objects.count(), order_count - 1)
        self.assertEqual(ProductOrderArchived.objects.filter(product__orchard__pk=self.orchard.pk).count(), 1)

    def test_product_selfpicking(self):
        product = Product.objects.create(
            title='Self-picking fruit', orchard=self.orchard,
            typ='self',
        )
        product.fruit_species.add(self.species)
        self.client.force_login(self.user)
        response = self.client.get(product.get_absolute_url())
        self.assertQuerySetEqual(response.context['trees'], [])
        # Only the tree with right availability and species should appear
        self.add_tree(self.orchard, availability='whole', species=self.species)
        self.add_tree(
            self.orchard, availability='picking',
            species=TreeSpecies.objects.create(name="any", fruit="any", category=self.category)
        )
        self.add_tree(self.orchard, availability='picking', species=self.species)
        response = self.client.get(product.get_absolute_url())
        self.assertQuerySetEqual(response.context['trees'], ['<Tree:  (pear)>'], transform=repr)
        # Not sure this is the good condition...
        year = date.today().year if (date.today().month < 9) else date.today().year + 1
        self.assertContains(
            response,
            '<strong>' + _("Fruit picking available around:") + '</strong> 01.09 – 15.09.%d' % year
        )
        # Test order confirmation
        client = User.objects.create_user(
            first_name='Nora', last_name='Schmied', street='Apfelstr. 11', pcode='5544',
            city='Birnheim', email="client@example.org", password='123456',
        )
        order = ProductOrder.objects.create(
            user=client, product=product, quantity=1, total_price=20,
            order_date=timezone.now(), status='ordered',
        )
        response = self.client.post(order.get_absolute_url(), data={'step': 'confirm'})
        # Email sent to client
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), ['client@example.org'])
        self.assertEqual(
            mail.outbox[0].body,
            '''Your order for the product "Self-picking fruit" (orchard "Test orchard") was confirmed by the provider.

Product: Self-picking fruit
Order date: {date}
Quantity: 1
Total price: 20.00 CHF

Paiement information:
CCP 25-25252525-0

We kindly ask you to bring this confirmation with you when you are going to self-pick to "Self-picking fruit".

After successfull self-picking, please give the above price amount to the provider according to the specified payment method.

Finally, two useful hints:
  * Picking fruits from a ladder is dangerous. It's about your health! Please consult our safety sheet (https://www.meinobstgarten.ch/about/safety/).
  * In an orchard, the grass under trees is also used. Please respect it and use marked paths.

Thanks for using the meinobstgarten.ch platform!
'''.format(date=date_format(order.order_date, 'DATETIME_FORMAT'))
        )

    def test_product_availability(self):
        prod = Product.objects.create(orchard=self.orchard, title="My product", availability=True)
        response = self.client.get(reverse('product_detail', args=[prod.orchard.pk, prod.pk]))
        self.assertNotContains(response, _("Sorry, this product is currently not available."))
        prod.availability = False
        prod.save()
        response = self.client.get(reverse('product_detail', args=[prod.orchard.pk, prod.pk]))
        self.assertContains(response, _("Sorry, this product is currently not available."))

    def test_product_list(self):
        private_orchard = Orchard.objects.create(
            name="Private", owner=self.user, is_public=False, municipality=self.location
        )
        Product.objects.create(orchard=self.orchard, title="My product", availability=True)
        Product.objects.create(orchard=self.orchard, title="My product unavail", availability=False)
        Product.objects.create(orchard=private_orchard, title="My product from private", availability=True)
        response = self.client.get(reverse('product_list'))
        self.assertQuerySetEqual(response.context['object_list'], ['<Product: My product>'], transform=repr)

    def test_own_product_list(self):
        other_user = User.objects.create_user(
            first_name='Aron', last_name='Apfelbaum', email="test2@example.org"
        )
        other_orchard = Orchard.objects.create(
            name="Other", owner=other_user, municipality=self.location
        )
        private_orchard = Orchard.objects.create(
            name="Private", owner=self.user, is_public=False, municipality=self.location
        )
        Product.objects.create(orchard=other_orchard, title="Product from other user", availability=True)
        Product.objects.create(orchard=self.orchard, title="My product unavail", availability=False)
        Product.objects.create(orchard=private_orchard, title="My product from private", availability=True)
        self.client.login(email=self.user.email, password='abcdef')
        response = self.client.get(reverse('my-products'))
        self.assertQuerySetEqual(
            response.context['object_list'],
            ['<Product: My product from private>', '<Product: My product unavail>'],
            transform=repr
        )

    def test_product_order(self):
        client = User.objects.create_user(
            first_name='Nora', last_name='Schmied', street='Apfelstr. 11', pcode='5544',
            city='Birnheim', email="client@example.org", password='123456',
        )

        prod = Product.objects.create(
            orchard=self.orchard, title="My product", availability=True, price='8.45',
            unit='500g', sending_costs='2.5',
        )
        prod_url = reverse('product_detail', args=[prod.orchard.pk, prod.pk])
        response = self.client.get(prod_url)
        self.assertNotContains(response, '<fieldset id="order_form">')
        self.client.login(email=client.email, password='123456')
        response = self.client.get(prod_url)
        self.assertContains(response, '<fieldset id="order_form">')
        response = self.client.post(prod_url, data={
            'user': client.pk, 'product': prod.pk, 'quantity': '2',
        })
        self.assertRedirects(response, prod_url)
        order = client.productorder_set.first()
        self.assertIsNotNone(order.order_date)
        self.assertEqual(float(order.total_price), 2 * 8.45 + 2.5)
        self.assertEqual(order.status, 'ordered')
        # Email sent to orchard contact
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].recipients(), ['test@example.org'])
        self.assertEqual(
            mail.outbox[0].body,
            '''An order was placed for your product "My product" (orchard "Test orchard").

Order by:
Nora Schmied
Apfelstr. 11
5544 Birnheim
client@example.org

Quantity: 2 * 500g

Price: CHF 19.40

Please go the following link to confirm availability and price of this order:
https://www.meinobstgarten.ch/orchard/{orch}/productorder/{ord}/
'''.format(orch=self.orchard.pk, ord=order.pk)
        )
        self.client.logout()

        # Order is not visible anonymously
        response = self.client.get(order.get_absolute_url())
        self.assertEqual(response.status_code, 403)

        # Now confirmation by orchard owner
        self.client.force_login(self.user)
        response = self.client.get(order.get_absolute_url())
        self.assertContains(response, _("Confirm"))
        response = self.client.post(order.get_absolute_url(), data={'step': 'confirm', 'remark': 'Die Früchte sind jetzt > reif!'}, follow=True)
        self.assertContains(response, _(
            "Thanks for the confirmation. An email was sent to the client "
            "with your paiement information."))
        order.refresh_from_db()
        self.assertEqual(order.status, 'confirmed')
        self.assertIsNotNone(order.confirm_date)
        # Email sent to client
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[1].recipients(), ['client@example.org'])
        self.assertEqual(
            mail.outbox[1].body,
            '''Your order for the product "My product" (orchard "Test orchard") was confirmed by the provider.

Product: My product
Order date: {date}
Quantity: 2 * 500g
Total price: 19.40 CHF
Remark: Die Früchte sind jetzt > reif!

Paiement information:
CCP 25-25252525-0

As soon as your paiement is received by the provider, he will send the product to your postal address:
Apfelstr. 11
5544 Birnheim

Thanks for using the meinobstgarten.ch platform!
'''.format(date=date_format(order.order_date, 'DATETIME_FORMAT'))
        )

    def test_product_order_form(self):
        """Unit test the form only."""
        client = User.objects.create_user(
            first_name='Nora', last_name='Schmied', street='Apfelstr. 11', pcode='5544',
            city='Birnheim', email="client@example.org", password='123456',
        )
        prod = Product.objects.create(
            orchard=self.orchard, title="My product", availability=True, price='8.45',
            sending_costs=None,
        )
        form = ProductOrderForm(
            initial={'product': prod, 'user': client},
            data={'user': client.pk, 'product': prod.pk, 'quantity': '10'}
        )
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(form.instance.status, 'ordered')
        self.assertEqual(form.instance.total_price, Decimal('84.5'))
        # No price
        prod.price = None
        prod.save()
        form = ProductOrderForm(
            initial={'product': prod, 'user': client},
            data={'user': client.pk, 'product': prod.pk, 'quantity': '10'}
        )
        self.assertTrue(form.is_valid())
        form.save()
        self.assertEqual(form.instance.total_price, 0)

    def test_product_order_cancel(self):
        client = User.objects.create_user(
            first_name='Nora', last_name='Schmied', street='Apfelstr. 11', pcode='5544',
            city='Birnheim', email="client@example.org", password='123456',
        )
        prod = Product.objects.create(
            orchard=self.orchard, title="My product", availability=True, price='8.45',
            sending_costs='2.5',
        )
        order = ProductOrder.objects.create(
            user=client, product=prod, quantity=1, total_price=20, order_date=timezone.now(),
            status='ordered',
        )
        # Cancellation by product orchard owner
        self.client.force_login(self.user)
        response = self.client.post(order.get_absolute_url(), data={
            'step': 'cancel', 'cancel_reason': "SORRY!" }, follow=True)
        order.refresh_from_db()
        self.assertEqual(order.status, 'cancelled')
        self.assertIsNotNone(order.cancel_date)
        # Email were sent to both parties
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].recipients(), ['client@example.org'])
        self.assertEqual(mail.outbox[1].recipients(), ['test@example.org'])
        self.assertEqual(
            mail.outbox[0].body,
            '''The order {id} for the product "My product" was unfortunately cancelled.

The reason was:
SORRY!
'''.format(id=order.order_id)
        )
        # Cancellation by product client
        order = ProductOrder.objects.create(
            user=client, product=prod, quantity=1, total_price=20, order_date=timezone.now(),
            status='ordered',
        )
        self.client.force_login(client)
        self.client.post(order.get_absolute_url(), data={
            'step': 'cancel', 'cancel_reason': "SORRY!",
        }, follow=True)
        order.refresh_from_db()
        self.assertEqual(order.status, 'cancelled')
        self.assertIsNotNone(order.cancel_date)
        # Email were sent to both parties
        self.assertEqual(len(mail.outbox), 4)
        self.assertEqual(mail.outbox[2].recipients(), ['client@example.org'])
        self.assertEqual(mail.outbox[3].recipients(), ['test@example.org'])

    def test_product_pending_mail(self):
        """
        Ordered products more than 3 days old trigger a reminder to site admin.
        """
        client = User.objects.create_user(
            first_name='Nora', last_name='Schmied', street='Apfelstr. 11', pcode='5544',
            city='Birnheim', email="client@example.org", password='123456',
        )
        product = Product.objects.create(
            orchard=self.orchard, title="My product", availability=True, price='8.45',
            unit='500g', sending_costs='2.5',
        )
        five_days_ago = timezone.now() - timezone.timedelta(days=5)
        ProductOrder.objects.bulk_create([
            ProductOrder(
                user=client, product=product, quantity=1, total_price=20,
                order_date=timezone.now(), status='ordered',
            ),
            ProductOrder(
                user=client, product=product, quantity=1, total_price=20,
                order_date=five_days_ago, status='cancelled',
            ),
            # Only this one should trigger the mail
            ProductOrder(
                user=client, product=product, quantity=3, total_price=20,
                order_date=five_days_ago, status='ordered',
            ),
        ])
        prod3 = ProductOrder.objects.get(quantity=3)

        DailyTasksCommand().send_products_reminders()
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].body,
            '''Pending product orders (more than 3 days without answer)
========================================================
- Nora Schmied ordered 'My product' (orchard 'Test orchard') on %(date)s
  https://www.meinobstgarten.ch%(url)s

''' % {'date': five_days_ago.strftime('%Y-%m-%d'), 'url': prod3.get_absolute_url()}
        )
